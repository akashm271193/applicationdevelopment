INSTALLATION: mvn archetype:generate -DarchetypeGroupId=io.dropwizard.archetypes -DarchetypeArtifactId=java-simple -DarchetypeVersion=2.0.10

DOCUMENTATION:

USE CASES:

1. 3 players playing the ttr game. They want random ticket generation. They will be communicating with the backend using an app/browser. They must login once. And we will be creating rooms (always will be present). Once a room is created, anyone can start a game. All tickets are generated. And then the game begins.

Notes : 
1. Tickets have score based on the shortest distance between the two locations on the ticket
2. Max of 30 tickets
3. 


Stack: 

1. Backend - Java
2. Tests - Junit, Cypress
3. Frontend - Angular 2
4. App - Android
5. Data Storage - Mysql, Redis