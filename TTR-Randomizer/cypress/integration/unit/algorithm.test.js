'use strict';

const Algorithm = require('../../../externaljs/algorithm.js');
const Path = require('../../../externaljs/classes/path.js');
const Node = require('../../../externaljs/classes/node.js');
const Connection = require('../../../externaljs/classes/connection.js');
const Pure = require('../../../externaljs/pure.js');

context('Pure Service', () => {
	it('Factorial function works as expected', () => {	
		assert.equal(Pure.factorial(0), 1, 'Factorial of 0 calculated correctly');
		assert.equal(Pure.factorial(1), 1, 'Factorial of 1 calculated correctly');
		assert.equal(Pure.factorial(2), 2, 'Factorial of 2 calculated correctly');
		assert.equal(Pure.factorial(3), 6, 'Factorial of 3 calculated correctly');
		assert.equal(Pure.factorial(4), 24, 'Factorial of 4 calculated correctly');
		assert.equal(Pure.factorial(5), 120, 'Factorial of 5 calculated correctly');
		assert.equal(Pure.factorial(10), 3628800, 'Factorial of 10 calculated correctly');
	});

	it('Max Available Tickets function works as expected', () => {	
		assert.equal(Pure.maxAvailableTickets(0), 0, 'Factorial of 0 calculated correctly');
		assert.equal(Pure.maxAvailableTickets(1), 0, 'Factorial of 1 calculated correctly');
		assert.equal(Pure.maxAvailableTickets(2), 1, 'Factorial of 2 calculated correctly');
		assert.equal(Pure.maxAvailableTickets(3), 3, 'Factorial of 3 calculated correctly');
		assert.equal(Pure.maxAvailableTickets(4), 6, 'Factorial of 4 calculated correctly');
		assert.equal(Pure.maxAvailableTickets(5), 10, 'Factorial of 5 calculated correctly');
		assert.equal(Pure.maxAvailableTickets(10), 45, 'Factorial of 10 calculated correctly');
	});
});

context('Algorithm', () => {
	context('Generate Connnections', () => {
		it('Can create valid connections', () => {
			var connection = new Connection(100, 1, 1);
			assert.exists(connection, 'Connection is initialized correctly');
			assert.equal(connection.nodeId, 100, 'Expected nodeId of connection is ok');
			assert.equal(connection.distance, 1, 'Expected distance of connection is ok');
			assert.equal(connection.routes, 1, 'Expected routs connection is ok');

			connection = new Connection("100", "1", "5");
			assert.exists(connection, 'Connection is initialized correctly');
			assert.equal(connection.nodeId, 100, 'Expected distance of connection is ok');
			assert.equal(connection.distance, 1, 'Expected distance of connection is ok');
			assert.equal(connection.routes, 5, 'Expected routs connection is ok');
		});

		it('Fails for various invalid inputs', () => {
			cy.fixture('failureInputs').then((data) => {
				var generateConnectionData = data['generateConnection'];
				if (!generateConnectionData) {
					assert.isOk(false, 'Input data for generating connections is missing');
				}

				Object.keys(generateConnectionData).forEach((key) => {
					var connection;
					try {
						connection = new Connection(...generateConnectionData[key]);
					} catch (ex) {
						assert.isOk(true, 'Error was thrown - ' + ex.error);
					}
					
					if (connection) {
						assert.isOk(false, 'Something is not right. Graph got generated when it was not expected to !!');
					}
				});
			}); 
		});
	});

	context('Generate Nodes', () => {
		it('Can create valid Nodes', () => {
			var node = new Node(1, 'Node 1');
			assert.exists(node, 'node is initialized correctly');
			assert.equal(node.nodeId, 1, 'Id of expected node is ok');
			assert.equal(node.nodeName, 'Node 1', 'Name of expected node is ok');
			expect(node.connectedNodes).to.deep.equal({});
		});

		it('Node Connection is added as expected', () => {
			const node = new Node(1, 'Node 1');
			const connections = [{ nodeId: 5, distance: 1, routes: 5}];
			node.addConnections(connections);
			assert.exists(node.connectedNodes, 'connections are initialized correctly');
			assert.exists(node.connectedNodes['5-1'], 'Connection present as expected');
			assert.equal(node.connectedNodes['5-1'].nodeId, 5, 'Connection nodeId present as expected');
			assert.equal(node.connectedNodes['5-1'].distance, 1, 'Connection distance present as expected');
			assert.equal(node.connectedNodes['5-1'].routes, 5, 'Connection routes present as expected');
		});

		it('Node Connection follows upsert methodology as expected', () => {
			const node = new Node(1, 'Node 1');
			const connections= [{ nodeId: 5, distance: 1, routes: 5}, { nodeId: 5, distance: 1, routes: 4}]
			node.addConnections(connections);
			assert.exists(node.connectedNodes, 'connections are initialized correctly');
			assert.exists(node.connectedNodes['5-1'], 'Connection present as expected');
			assert.equal(node.connectedNodes['5-1'].nodeId, 5, 'Connection distance present as expected');
			assert.equal(node.connectedNodes['5-1'].distance, 1, 'Connection distance present as expected');
			assert.equal(node.connectedNodes['5-1'].routes, 4, 'Connection routes present as expected');
		})

		it('Multiple node connections can be added as expected', () => {
			const node = new Node(1, 'Node 1');
			const connections= [{ nodeId: 5, distance: 1, routes: 5}, { nodeId: 5, distance: 2, routes: 1}, { nodeId: 100, distance: 1, routes: 5}]
			node.addConnections(connections);
			assert.exists(node.connectedNodes, 'connections are initialized correctly');
			assert.exists(node.connectedNodes['5-1'], 'Connection present as expected');
			assert.equal(node.connectedNodes['5-1'].nodeId, 5, 'Connection nodeId present as expected');
			assert.equal(node.connectedNodes['5-1'].distance, 1, 'Connection distance present as expected');
			assert.equal(node.connectedNodes['5-1'].routes, 5, 'Connection routes present as expected');


			assert.exists(node.connectedNodes, 'connections are initialized correctly');
			assert.exists(node.connectedNodes['5-2'], 'Connection present as expected');
			assert.equal(node.connectedNodes['5-2'].nodeId, 5, 'Connection nodeId present as expected');
			assert.equal(node.connectedNodes['5-2'].distance, 2, 'Connection distance present as expected');
			assert.equal(node.connectedNodes['5-2'].routes, 1, 'Connection routes present as expected');

			assert.exists(node.connectedNodes, 'connections are initialized correctly');
			assert.exists(node.connectedNodes['100-1'], 'Connection present as expected');
			assert.equal(node.connectedNodes['100-1'].nodeId, 100, 'Connection nodeId present as expected');
			assert.equal(node.connectedNodes['100-1'].distance, 1, 'Connection distance present as expected');
			assert.equal(node.connectedNodes['100-1'].routes, 5, 'Connection routes present as expected');
		})

		it('Fails for various invalid inputs', () => {
			cy.fixture('failureInputs').then((data) => {
				var generateNodeData = data['generateNode'];
				if (!generateNodeData) {
					assert.isOk(false, 'Input data for generating nodes is missing');
				}

				Object.keys(generateNodeData).forEach((key) => {
					var node;
					try {
						node = new Node(...generateNodeData[key]);
					} catch (ex) {
						assert.isOk(true, 'Error was thrown - ' + ex.error);
					}
					
					if (node) {
						assert.isOk(false, 'Something is not right. Node got generated when it was not expected to !!');
					}
				});

				var generateNodeConnectionData = data['generateNodeConnection'];
				if (!generateNodeConnectionData) {
					assert.isOk(false, 'Input data for generating nodes is missing');
				}

				Object.keys(generateNodeConnectionData).forEach((key) => {
					var node = new Node(1, 'Node 1');
					var connectionsAdded = false;
					try {
						node.addConnections(generateNodeConnectionData[key]);
						connectionsAdded = true;
					} catch (ex) {
						assert.isOk(true, 'Error was thrown - ' + ex.error);
					}
					
					if (connectionsAdded) {
						assert.isOk(false, 'Something is not right. Node got generated when it was not expected to for input : ' 
							+ JSON.stringify(generateNodeConnectionData[key]));
					}
				});
			}); 
		});
	});

	context('Generate Paths', () => {
		it('Can create valid Paths', () => {
			var path = new Path(1, 2, 1, 2);
			assert.exists(path, 'path is initialized correctly');
			assert.equal(path.nodeId1, 1, 'Node Id 1 of expected path is ok');
			assert.equal(path.nodeId2, 2, 'Node Id 2 of expected path is ok');
			assert.equal(path.distance, 1, 'Distance of expected path is ok');
			assert.equal(path.routes, 2, 'Routes count of expected path is ok');
		});

		it('Unique Id generation works as expected', () => {
			var path = new Path(1, 2, 2, 3);
			assert.exists(path, 'path is initialized correctly');
			assert.equal(path.generateUniquePathId, '1-2-2', 'Path 1 generate as expected');

			path = new Path(2, 1, 2, 3);
			assert.exists(path, 'path is initialized correctly');
			assert.equal(path.generateUniquePathId, '1-2-2', 'Path 2 generate as expected');

			path = new Path(-1, 2, 2, 3);
			assert.exists(path, 'path is initialized correctly');
			assert.equal(path.generateUniquePathId, '-1-2-2', 'Path 3 generate as expected');

			path = new Path(5, -2, 3, 2);
			assert.exists(path, 'path is initialized correctly');
			assert.equal(path.generateUniquePathId, '-2-5-3', 'Path 4 generate as expected');

			path = new Path(-5, -2, 3, 2);
			assert.exists(path, 'path is initialized correctly');
			assert.equal(path.generateUniquePathId, '-5--2-3', 'Path 5 generate as expected');

			path = new Path(-2, -5, 3, 2);
			assert.exists(path, 'path is initialized correctly');
			assert.equal(path.generateUniquePathId, '-5--2-3', 'Path 6 generate as expected');
		});

		it('Fails for various invalid inputs', () => {
			cy.fixture('failureInputs').then((data) => {
				var generatePathData = data['generatePath'];
				Object.keys(generatePathData).forEach((key) => {
					var path;
					try {
						path = new Path(...generatePathData[key]);
					} catch (ex) {
						assert.isOk(true, 'Error was thrown - ' + ex.error);
					}
					
					if (path) {
						assert.isOk(false, 'Something is not right. Graph got generated when it was not expected to !!');
					}
				});
			}); 
		});
	});



	context('Generate Graph', () => {
		/***
		Expected input ::
		[{
			nodeId:,
			nodeName:,
			connections: [{
				distance: ,
				routes:
			}]
		}];
		***/
		it('Can create an empty graph', () => {
			var graph = Algorithm.createGraph();
			assert.exists(graph, 'graph is a valid object');
			assert.exists(graph.getAllNodes, 'graph is initialized with all nodes');
			assert.equal(graph.getAllNodes.length, 0, 'Node count as expected');
			assert.exists(graph.getAllPaths, 'graph is initialized with all paths');
			expect(graph.getAllPaths).to.deep.equal({});
		});

		it('Can create a graph from a list of isolated nodes', () => {
			var input = [{
				nodeId: 1,
				nodeName: 'Vancouver',
				connections: []
			}, {
				nodeId: 2,
				nodeName: 'Seattle',
				connections: []
			}];
			var graph = Algorithm.createGraph(input);
			assert.exists(graph, 'graph is a valid object');
			assert.exists(graph.getAllNodes, 'graph is initialized with all nodes');
			var nodes = graph.getAllNodes;
			assert.equal(nodes.length, 2, 'Node count as expected');
			assert.equal(nodes[0].nodeId, 1, '1st Node -> Id as expected');
			assert.equal(nodes[0].nodeName,'Vancouver', '1st Node -> Name as expected');
			expect(nodes[0].connectedNodes).to.deep.equal({});
			assert.equal(nodes[1].nodeId,2, '2nd Node -> Id as expected');
			assert.equal(nodes[1].nodeName,'Seattle', '2nd Node -> Name as expected');
			expect(nodes[1].connectedNodes).to.deep.equal({});
			var paths = graph.getAllPaths;
			assert.exists(paths, 'graph is initialized with all paths');
			expect(paths).to.deep.equal({});
		});

		it('Can create a graph from a list of 2 nodes', () => {
			var input = [{
					nodeId: 1,
					nodeName: 'Vancouver',
					connections: [{
						nodeId: 2, 
						distance: 1,
						routes: 2
					}]
				}, {
					nodeId: 2,
					nodeName: 'Seattle',
					connections: [{
						nodeId: 1, 
						distance: 1,
						routes: 2
					}]
				}];
			var graph = Algorithm.createGraph(input);
			var expectedConnectionsForNode1 = {
				'2-1': { 
					nodeId: 2,
					distance: 1, 
					routes: 2 
				}
			};
			var expectedConnectionsForNode2 = {
				'1-1': {
					nodeId: 1,
					distance: 1, 
					routes: 2 
				}
			};
			assert.exists(graph, 'graph is a valid object');
			assert.exists(graph.getAllNodes, 'graph is initialized with all nodes');
			var nodes = graph.getAllNodes;
			assert.equal(nodes.length, 2, 'Node count as expected');
			assert.equal(nodes[0].nodeId, 1, '1st Node -> Id as expected');
			assert.equal(nodes[0].nodeName,'Vancouver', '1st Node -> Name as expected');
			expect(nodes[0].connectedNodes).to.deep.equal(expectedConnectionsForNode1);
			assert.equal(nodes[1].nodeId,2, '2nd Node -> Id as expected');
			assert.equal(nodes[1].nodeName,'Seattle', '2nd Node -> Name as expected');
			expect(nodes[1].connectedNodes).to.deep.equal(expectedConnectionsForNode2);
			var paths = graph.getAllPaths;
			assert.exists(paths, 'graph is initialized with all paths');
			assert.equal(Object.keys(paths).length, 1, 'Path count as expected');
			assert.isOk(paths['1-2-1'], 'expected path exists');
			assert.equal(paths['1-2-1'].nodeId1, 1, 'Node Id 1 of expected path is ok');
			assert.equal(paths['1-2-1'].nodeId2, 2, 'Node Id 2 of expected path is ok');
			assert.equal(paths['1-2-1'].distance, 1, 'Distance of expected path is ok');
			assert.equal(paths['1-2-1'].routes, 2, 'Routes count of expected path is ok');
		});


		it('Can create a graph from a list of multiple nodes', () => {
			var input = [{
					nodeId: 1,
					nodeName: 'Vancouver',
					connections: [{
						nodeId: 2, 
						distance: 1,
						routes: 2
					}]
				}, {
					nodeId: 2,
					nodeName: 'Seattle',
					connections: [{
						nodeId: 1, 
						distance: 1,
						routes: 2
					}]
				}, {
					nodeId: 3,
					nodeName: 'Calgary',
					connections: [{
						nodeId: 1, 
						distance: 3,
						routes: 1
					}, {
						nodeId: 2, 
						distance: 4,
						routes: 1
					}]
				}, {
					nodeId: 4,
					nodeName: 'Helena',
					connections: [{
						nodeId: 3, 
						distance: 4,
						routes: 1
					}, {
						nodeId: 2, 
						distance: 6,
						routes: 1
					}]
				}];
			var graph = Algorithm.createGraph(input);
			var expectedConnectionsForNode1 = {
				'2-1': { 
					nodeId: 2,
					distance: 1, 
					routes: 2 
				}
			};
			var expectedConnectionsForNode2 = {
				'1-1': {
					nodeId: 1,
					distance: 1, 
					routes: 2 
				}
			};
			assert.exists(graph, 'graph is a valid object');
			assert.exists(graph.getAllNodes, 'graph is initialized with all nodes');
			const nodes = graph.getAllNodes;
			assert.equal(nodes.length, 4, 'Node count as expected');

			// Check that all nodes are present
			const node1 = nodes.find((node) => node.nodeId == 1);
			assert.equal(node1.nodeId, 1, '1st Node -> Id as expected');
			assert.equal(node1.nodeName,'Vancouver', '1st Node -> Name as expected');

			const node2 = nodes.find((node) => node.nodeId == 2);
			assert.equal(node2.nodeId, 2, '2nd Node -> Id as expected');
			assert.equal(node2.nodeName,'Seattle', '2nd Node -> Name as expected')

			const node3 = nodes.find((node) => node.nodeId == 3);
			assert.equal(node3.nodeId, 3, '3rd Node -> Id as expected');
			assert.equal(node3.nodeName,'Calgary', '3rd Node -> Name as expected');

			const node4 = nodes.find((node) => node.nodeId == 4);
			assert.equal(node4.nodeId, 4, '4th Node -> Id as expected');
			assert.equal(node4.nodeName,'Helena', '4th Node -> Name as expected');


			// Ensure that all paths are generated
			var paths = graph.getAllPaths;
			assert.exists(paths, 'graph is initialized with all paths');
			assert.equal(Object.keys(paths).length, 5, 'Path count as expected');

			assert.isOk(paths['1-2-1'], 'expected path exists');
			assert.equal(paths['1-2-1'].routes, 2, 'Routes count of expected path is ok');

			assert.isOk(paths['1-3-3'], 'expected path exists');
			assert.equal(paths['1-3-3'].routes, 1, 'Routes count of expected path is ok');

			assert.isOk(paths['2-3-4'], 'expected path exists');
			assert.equal(paths['2-3-4'].routes, 1, 'Routes count of expected path is ok');

			assert.isOk(paths['3-4-4'], 'expected path exists');
			assert.equal(paths['3-4-4'].routes, 1, 'Routes count of expected path is ok');

			assert.isOk(paths['2-4-6'], 'expected path exists');
			assert.equal(paths['2-4-6'].routes, 1, 'Routes count of expected path is ok');
		});
		
		it('Fails for various invalid inputs', () => {
			cy.fixture('failureInputs').then((data) => {
				var generateGraphData = data['generateGraph'];
				Object.keys(generateGraphData).forEach((key) => {
					var graph;
					try {
						graph = Algorithm.createGraph(generateGraphData[key]);
					} catch (ex) {
						assert.isOk(true, 'Error was thrown - ' + ex.error);
					}
					
					if (graph) {
						assert.isOk(false, 'Something is not right. Graph got generated when it was not expected to !!');
					}
				});
			}); 
		});
	});

	context('Generate Tickets', () => {
		/***
		Expected input ::
		[{
			nodeId:,
			nodeName:,
			connections: [{
				distance: ,
				routes:
			}]
		}];
		***/

		it('Can create all possible tickets with 4 nodes', () => {
			var input = [{
					nodeId: 1,
					nodeName: 'Vancouver',
					connections: [{
						nodeId: 2, 
						distance: 1,
						routes: 2
					}]
				}, {
					nodeId: 2,
					nodeName: 'Seattle',
					connections: [{
						nodeId: 1, 
						distance: 1,
						routes: 2
					}]
				}, {
					nodeId: 3,
					nodeName: 'Calgary',
					connections: [{
						nodeId: 1, 
						distance: 3,
						routes: 1
					}, {
						nodeId: 2, 
						distance: 4,
						routes: 1
					}]
				}, {
					nodeId: 4,
					nodeName: 'Helena',
					connections: [{
						nodeId: 3, 
						distance: 4,
						routes: 1
					}, {
						nodeId: 2, 
						distance: 6,
						routes: 1
					}]
			}];


			var graph = Algorithm.createGraph(input);
			assert.exists(graph, 'graph is a valid object');
			assert.exists(graph.getAllNodes, 'graph is initialized with all nodes');
			const nodes = graph.getAllNodes;
			assert.equal(nodes.length, 4, 'Node count as expected');


			// Ensure that all paths are generated
			var paths = graph.getAllPaths;
			assert.exists(paths, 'graph is initialized with all paths');
			assert.equal(Object.keys(paths).length, 5, 'Path count as expected');

			const  allTickets = Algorithm.generateAllTickets(graph);
			let maxPossibleTickets = Pure.maxAvailableTickets(nodes.length);

			assert.equal(Object.keys(allTickets).length, 6, "Tickets generated as expected");
			// console.log(allTickets);

		});

		it('Can create all possible tickets with 5 nodes', () => {
			var input = [{
					nodeId: 1,
					nodeName: 'Vancouver',
					connections: [{
						nodeId: 2, 
						distance: 1,
						routes: 2
					}, {
						nodeId: 5, 
						distance: 10,
						routes: 1
					}, {
						nodeId: 3, 
						distance: 3,
						routes: 1
					}]
				}, {
					nodeId: 2,
					nodeName: 'Seattle',
					connections: [{
						nodeId: 1, 
						distance: 1,
						routes: 2
					}, {
						nodeId: 3, 
						distance: 4,
						routes: 1,
					}, {
						nodeId: 4, 
						distance: 5,
						routes: 1,
					}]
				}, {
					nodeId: 3,
					nodeName: 'Calgary',
					connections: [{
						nodeId: 1, 
						distance: 3,
						routes: 1
					}, {
						nodeId: 2, 
						distance: 4,
						routes: 1
					}]
				}, {
					nodeId: 4,
					nodeName: 'Helena',
					connections: [{
						nodeId: 3, 
						distance: 4,
						routes: 1
					}, {
						nodeId: 2, 
						distance: 5,
						routes: 1
					}]
				}, {
					nodeId: 5,
					nodeName: 'New York',
					connections: [{
						nodeId: 1, 
						distance: 10,
						routes: 1
					}]
				}];

			var graph = Algorithm.createGraph(input);
			assert.exists(graph, 'graph is a valid object');
			assert.exists(graph.getAllNodes, 'graph is initialized with all nodes');
			const nodes = graph.getAllNodes;
			assert.equal(nodes.length, 5, 'Node count as expected');


			// Ensure that all paths are generated
			var paths = graph.getAllPaths;
			assert.exists(paths, 'graph is initialized with all paths');
			assert.equal(Object.keys(paths).length, 6, 'Path count as expected');

			const  allTickets = Algorithm.generateAllTickets(graph);
			let maxPossibleTickets = Pure.maxAvailableTickets(nodes.length);

			assert.equal(Object.keys(allTickets).length, 10, "Tickets generated as expected");
			// console.log(allTickets);

		});

		it('Can create all possible tickets with 3 nodes and shortest path', () => {
			var input = [{
					nodeId: 1,
					nodeName: 'Vancouver',
					connections: [{
						nodeId: 2, 
						distance: 1,
						routes: 2
					}, {
						nodeId: 3,
						distance: 3,
						routes:1
					}]
				}, {
					nodeId: 2,
					nodeName: 'Seattle',
					connections: [{
						nodeId: 1, 
						distance: 1,
						routes: 2
					}, {
						nodeId: 3, 
						distance: 4,
						routes: 1
					} ]
				}, {
					nodeId: 3,
					nodeName: 'Calgary',
					connections: [{
						nodeId: 1, 
						distance: 3,
						routes: 1
					}, {
						nodeId: 2, 
						distance: 4,
						routes: 1
					}]
				}];


			var graph = Algorithm.createGraph(input);
			assert.exists(graph, 'graph is a valid object');
			assert.exists(graph.getAllNodes, 'graph is initialized with all nodes');
			const nodes = graph.getAllNodes;
			assert.equal(nodes.length, 3, 'Node count as expected');


			// Ensure that all paths are generated
			var paths = graph.getAllPaths;
			assert.exists(paths, 'graph is initialized with all paths');
			assert.equal(Object.keys(paths).length, 3, 'Path count as expected');

			const  allTickets = Algorithm.generateAllTickets(graph);
			let maxPossibleTickets = Pure.maxAvailableTickets(nodes.length);

			assert.equal(Object.keys(allTickets).length, 3, "Tickets generated as expected");
			assert.equal(allTickets['1-3'], 3, "Shortest distance calculated"); 
			// console.log(allTickets);
		});


		it('Can create all possible tickets with 12 nodes and shortest path', () => {
			var input = [
				{
					nodeId: 1,
					nodeName: 'Vancouver',
					connections: [
						{
							nodeId: 2, 
							distance: 2,
							routes: 1
						}, 
						{
							nodeId: 7,
							distance: 3,
							routes:1
						},
						{
							nodeId: 9,
							distance: 10,
							routes:1
						}
					]
				},
				{
					nodeId: 2,
					nodeName: 'Seattle',
					connections: [
						{
							nodeId: 3, 
							distance: 1,
							routes: 1
						}, 
						{
							nodeId: 1,
							distance: 2,
							routes:1
						},
						{
							nodeId: 6,
							distance: 4,
							routes:1
						}
					]
				},
				{
					nodeId: 5,
					nodeName: 'Las Vegas',
					connections: [
						{
							nodeId: 4, 
							distance: 2,
							routes: 1
						}, 
						{
							nodeId: 6,
							distance: 3,
							routes:1
						},
					]
				},
				{
					nodeId: 11,
					nodeName: 'Miami',
					connections: [
						{
							nodeId: 12, 
							distance: 6,
							routes: 1
						}, 
						{
							nodeId: 6,
							distance: 5,
							routes:1
						},
						{
							nodeId: 10,
							distance: 5,
							routes:1
						}
					]
				},
				{
					nodeId: 12,
					nodeName: 'Texas',
					connections: [
						{
							nodeId: 4, 
							distance: 6,
							routes: 1
						}, 
						{
							nodeId: 11,
							distance: 6,
							routes:1
						},
					]
				},
				{
					nodeId: 4,
					nodeName: 'Los Angeles',
					connections: [
						{
							nodeId: 3, 
							distance: 5,
							routes: 1
						}, 
						{
							nodeId: 12,
							distance: 6,
							routes:1
						},
						{
							nodeId: 5,
							distance: 2,
							routes:1
						},
					]
				},
				{
					nodeId: 3,
					nodeName: 'Portland',
					connections: [
						{
							nodeId: 2, 
							distance: 1,
							routes: 1
						}, 
						{
							nodeId: 4,
							distance: 5,
							routes:1
						},
					]
				},
				{
					nodeId: 6,
					nodeName: 'Helena',
					connections: [
						{
							nodeId: 2, 
							distance: 4,
							routes: 1
						}, 
						{
							nodeId: 7,
							distance: 4,
							routes:1
						},
						{
							nodeId: 5,
							distance: 3,
							routes:1
						},
						{
							nodeId: 11,
							distance: 5,
							routes:1
						},
					]
				},
				{
					nodeId: 7,
					nodeName: 'Calgary',
					connections: [
						{
							nodeId: 1, 
							distance: 3,
							routes: 1
						}, 
						{
							nodeId: 8,
							distance: 5,
							routes:1
						},
						{
							nodeId: 6,
							distance: 4,
							routes:1
						},
					]
				},
				{
					nodeId: 10,
					nodeName: 'New York',
					connections: [
						{
							nodeId: 8, 
							distance: 2,
							routes: 1
						}, 
						{
							nodeId: 9,
							distance: 3,
							routes:1
						},
						{
							nodeId: 11,
							distance: 5,
							routes:1
						},
					]
				},
				{
					nodeId: 8,
					nodeName: 'Toronto',
					connections: [
						{
							nodeId: 7, 
							distance: 5,
							routes: 1
						}, 
						{
							nodeId: 9,
							distance: 3,
							routes:1
						},
						{
							nodeId: 10,
							distance: 2,
							routes:1
						},
					]
				},
				{
					nodeId: 9,
					nodeName: 'Montreal',
					connections: [
						{
							nodeId: 1, 
							distance: 10,
							routes: 1
						}, 
						{
							nodeId: 8,
							distance: 3,
							routes:1
						},
						{
							nodeId: 10,
							distance: 3,
							routes:1
						},
					]
				},
			];

			var graph = Algorithm.createGraph(input);
			assert.exists(graph, 'graph is a valid object');
			assert.exists(graph.getAllNodes, 'graph is initialized with all nodes');
			const nodes = graph.getAllNodes;
			assert.equal(nodes.length, 12, 'Node count as expected');


			// Ensure that all paths are generated
			var paths = graph.getAllPaths;
			assert.exists(paths, 'graph is initialized with all paths');
			// assert.equal(Object.keys(paths).length, 3, 'Path count as expected');

			const  allTickets = Algorithm.generateAllTickets(graph);
			let maxPossibleTickets = Pure.maxAvailableTickets(nodes.length);

			assert.equal(Object.keys(allTickets).length, 66, "Tickets generated as expected");
			assert.equal(allTickets['1-2'], 2, "Shortest distance calculated"); 
			assert.equal(allTickets['1-6'], 6, "Shortest distance calculated");
			assert.equal(allTickets['2-5'], 7, "Shortest distance calculated");
			assert.equal(allTickets['1-11'], 11, "Shortest distance calculated");
			assert.equal(allTickets['8-11'], 7, "Shortest distance calculated");
			assert.equal(allTickets['4-11'], 10, "Shortest distance calculated");
			assert.equal(allTickets['2-9'], 12, "Shortest distance calculated"); 
			assert.equal(allTickets['2-10'], 12, "Shortest distance calculated");
			assert.equal(allTickets['4-9'], 17, "Shortest distance calculated");
			assert.equal(allTickets['6-9'], 12, "Shortest distance calculated");
			// console.log(allTickets);
		});
	});
});