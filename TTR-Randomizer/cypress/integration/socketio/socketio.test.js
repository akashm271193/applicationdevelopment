'use strict';

const socketIOClient = require("socket.io-client");

context('SocketIO', () => {
	const socketQueryParams = {};
	let roomId = '';
	let pin = '';

	context('Plain socket IO connection', () => {
		it('Should not connect to socket io server any queryParams', () => {	
			cy.createSocketConnection("ws://localhost:3000/", { 
				closeOnConnect: true,
				failOnError: false,
			}).then((resp) => {
				expect(resp.status).to.equal(false);
			});
		});

		// Need better error messages on socket io for the tests below

		/** 

		it('Should not connect to socket io server without magicLink', () => {	
			cy.createSocketConnection("ws://localhost:3000/", { 
				closeOnConnect: true,
				failOnError: false,
			}).then((resp) => {
				expect(resp.status).to.equal(false);
			});
		});
		
		it('Should not connect to socket io server without room', () => {	
			cy.createSocketConnection("ws://localhost:3000/", { 
				closeOnConnect: true,
				failOnError: false,
			}).then((resp) => {
				expect(resp.status).to.equal(false);
			});
		});

		*/
	});

	context('SocketIO connection after creating room', () => {
		it('Should connect to socket io server with magicLink after creating room', () => {
			cy.request({
				method: "POST", 
				url: "localhost:8084/api/createNewRoom",
				body: {
					roomName: `${Date.now()}`,
					userName: "CypressUser"
				}
			}).then((xhrResponse) => {
				const body = xhrResponse.body;
				const queryParams = {
					roomId: body.roomId,
					magicLink: body.magicLink,
					userUuid: body.userUuid,
				};
				cy.createSocketConnection("ws://localhost:3000/", {
					closeOnConnect: true,
					queryParams: queryParams
				}).then((resp) => {
					expect(resp.status).to.equal(true);
					socketQueryParams.roomId = queryParams.roomId;
					socketQueryParams.magicLink = queryParams.magicLink;
				});
			});
		});

		it('Should not be able reconnect to socket io server with a previous successful magicLink', () => {
			expect(socketQueryParams).to.exist;
			expect(socketQueryParams.roomId).to.exist;
			expect(socketQueryParams.magicLink).to.exist;
			
			cy.createSocketConnection("ws://localhost:3000/", {
				closeOnConnect: true,
				queryParams: socketQueryParams,
				failOnError: false,
			}).then((resp) => {
				expect(resp.status).to.be.false;
			});
		});
	});

	
	context('SocketIO connection after joining room', () => {
		it('Should connect to socket io server with magicLink after joining room', () => {
			cy.request({
				method: "POST", 
				url: "localhost:8084/api/createNewRoom",
				body: {
					roomName: `${Date.now()}`,
					userName: "CypressUser"
				}
			}).then((createRoomResponse) => {
				const body = createRoomResponse.body;
				cy.wrap(body.roomId).as('newRoomId');
				cy.wrap(body.pin).as('roomPin');
			});

			cy.get('@newRoomId').then(newRoomId => {
				cy.get('@roomPin').then(roomPin => {
					cy.request({
						method: "POST", 
						url: "localhost:8084/api/joinRoom",
						body: {
							roomId: newRoomId,
							pin: roomPin,
							userName: `CypressUser${Date.now()/1000}`
						}
					}).then((joinRoomResponse) => {
						const newBody = joinRoomResponse.body;
						const queryParams = {
							roomId: newRoomId,
							magicLink: newBody.magicLink,
							userUuid: newBody.userUuid,
						};
						cy.createSocketConnection("ws://localhost:3000/", {
							closeOnConnect: true,
							queryParams: queryParams
						}).then((resp) => {
							expect(resp.status).to.equal(true);
							socketQueryParams.roomId = queryParams.roomId;
							socketQueryParams.magicLink = queryParams.magicLink;
						});
					});
				});
			})
		});

		it('Should not be able reconnect to socket io server with a previous successful magicLink', () => {
			expect(socketQueryParams).to.exist;
			expect(socketQueryParams.roomId).to.exist;
			expect(socketQueryParams.magicLink).to.exist;

			cy.createSocketConnection("ws://localhost:3000/", {
				closeOnConnect: true,
				queryParams: socketQueryParams,
				failOnError: false,
			}).then((resp) => {
				expect(resp.status).to.be.false;
			});
		});
		
	});


	context('Start Game', () => {
		describe('1 Player', () => {
			it('Should be able to start game with 1 player after creating room', () => {
				cy.createNewRoom().then((createRoomResponse) => {
					const body = createRoomResponse.body;
					cy.wrap(body.roomId).as('newRoomId');
					cy.wrap(body.pin).as('roomPin');

					const queryParams = {
						roomId: body.roomId,
						magicLink: body.magicLink,
						userUuid: body.userUuid,
					};

					cy.createSocketConnection("ws://localhost:3000/", {
						closeOnConnect: false,
						queryParams: queryParams
					}).as('socketIoResponse');
				});

				cy.get('@socketIoResponse').then((socketIoResponse) => {
					expect(socketIoResponse.status).to.equal(true);
					expect(socketIoResponse.socket).to.exist;
					expect(socketIoResponse.socket.connected).to.equal(true);
					
					let roomId;
					cy.get('@newRoomId').then(roomIdVal => {
						cy.emitEvent(socketIoResponse.socket, {
							eventName: 'startGame',
							eventParams: {
								roomId: roomIdVal,
								gameId: 1 
							}
						}).then((startGameresp) => {
							expect(startGameresp?.status).to.be.true;
						});

						cy.emitEvent(socketIoResponse.socket, {
							eventName: 'startGame',
							eventParams: {
								roomId: roomIdVal,
								gameId: 1 
							}
						}).then((startGameresp) => {
							expect(startGameresp?.status).to.be.false;
						});
					});
				});
			});

			it('Should be not be able to start another game after starting one', () => {
				cy.createNewRoom().then((createRoomResponse) => {
					const body = createRoomResponse.body;
					cy.wrap(body.roomId).as('newRoomId');
					cy.wrap(body.pin).as('roomPin');

					const queryParams = {
						roomId: body.roomId,
						magicLink: body.magicLink,
						userUuid: body.userUuid,
					};

					cy.createSocketConnection("ws://localhost:3000/", {
						closeOnConnect: false,
						queryParams: queryParams
					}).as('socketIoResponse');
				});

				cy.get('@socketIoResponse').then((socketIoResponse) => {
					expect(socketIoResponse.status).to.equal(true);
					expect(socketIoResponse.socket).to.exist;
					expect(socketIoResponse.socket.connected).to.equal(true);
					
					let roomId;
					cy.get('@newRoomId').then(roomIdVal => {
						cy.emitEvent(socketIoResponse.socket, {
							eventName: 'startGame',
							eventParams: {
								roomId: roomIdVal,
								gameId: 1 
							}
						}).then((startGameresp) => {
							expect(startGameresp?.status).to.be.true;
						});

						cy.emitEvent(socketIoResponse.socket, {
							eventName: 'startGame',
							eventParams: {
								roomId: roomIdVal,
								gameId: 1 
							}
						}).then((startGameresp) => {
							expect(startGameresp?.status).to.be.false;
						});
					});
				});
			});
		});

		describe('2 Players', () => {
			it('Should be able to start game after all players are in the room', () => {
				cy.createNewRoom().then((createRoomResponse) => {
					const body = createRoomResponse.body;
					cy.wrap(body.roomId).as('newRoomId');
					cy.wrap(body.pin).as('roomPin');

					const queryParams = {
						roomId: body.roomId,
						magicLink: body.magicLink,
						userUuid: body.userUuid,
					};

					cy.createSocketConnection("ws://localhost:3000/", {
						closeOnConnect: false,
						queryParams: queryParams
					}).as('socketIoResponse');
				});

				cy.get('@socketIoResponse').then((socketIoResponse) => {
					expect(socketIoResponse.status).to.equal(true);
					expect(socketIoResponse.socket).to.exist;
					expect(socketIoResponse.socket.connected).to.equal(true);

					let roomId;
					cy.get('@newRoomId').then(newRoomId => {
						cy.get('@roomPin').then(roomPin => {
							cy.request({
								method: "POST", 
								url: "localhost:8084/api/joinRoom",
								body: {
									roomId: newRoomId,
									pin: roomPin,
									userName: `CypressUser${Date.now()/1000}`
								}
							}).then((joinRoomResponse) => {
								const newBody = joinRoomResponse.body;
								const queryParams = {
									roomId: newRoomId,
									magicLink: newBody.magicLink,
									userUuid: newBody.userUuid,
								};
								cy.createSocketConnection("ws://localhost:3000/", {
									closeOnConnect: false,
									queryParams: queryParams
								}).as('socketIoResponse2');


								cy.get('@socketIoResponse2').then((socketIoResponse2) => {
									expect(socketIoResponse2.status).to.equal(true);
									expect(socketIoResponse2.socket).to.exist;
									expect(socketIoResponse2.socket.connected).to.equal(true);
									cy.startGameWithMultiSockets([socketIoResponse.socket, socketIoResponse2.socket], {
										eventName: 'startGame',
										eventParams: {
											roomId: newRoomId,
											gameId: 1 
										}
									}).then((startGameresp) => {
										expect(startGameresp?.status).to.be.true;
									});
								});
							});
						});
					});
				});
			});
		});
		
	});


	context('Join Game', () => {
		it('Should be able to join game after creating room', () => {
			cy.createNewRoom().then((createRoomResponse) => {
				const body = createRoomResponse.body;
				cy.wrap(body.roomId).as('newRoomId');
				cy.wrap(body.pin).as('roomPin');

				const queryParams = {
					roomId: body.roomId,
					magicLink: body.magicLink,
					userUuid: body.userUuid,
				};

				cy.createSocketConnection("ws://localhost:3000/", {
					closeOnConnect: false,
					queryParams: queryParams
				}).as('socketIoResponse');
			});

			cy.get('@socketIoResponse').then((socketIoResponse) => {
				expect(socketIoResponse.status).to.equal(true);
				expect(socketIoResponse.socket).to.exist;
				expect(socketIoResponse.socket.connected).to.equal(true);
				
				let roomId;
				cy.get('@newRoomId').then(roomIdVal => {
					cy.emitEvent(socketIoResponse.socket, {
						eventName: 'startGame',
						eventParams: {
							roomId: roomIdVal,
							gameId: 1 
						}
					}).then((startGameresp) => {
						expect(startGameresp?.status).to.be.true;
					});

					cy.emitEvent(socketIoResponse.socket, {
						eventName: 'startGame',
						eventParams: {
							roomId: roomIdVal,
							gameId: 1 
						}
					}).then((startGameresp) => {
						expect(startGameresp?.status).to.be.false;
					});
				});
			});
		});

		it('Should be not be able to start another game after starting one', () => {
			cy.createNewRoom().then((createRoomResponse) => {
				const body = createRoomResponse.body;
				cy.wrap(body.roomId).as('newRoomId');
				cy.wrap(body.pin).as('roomPin');

				const queryParams = {
					roomId: body.roomId,
					magicLink: body.magicLink,
					userUuid: body.userUuid,
				};

				cy.createSocketConnection("ws://localhost:3000/", {
					closeOnConnect: false,
					queryParams: queryParams
				}).as('socketIoResponse');
			});

			cy.get('@socketIoResponse').then((socketIoResponse) => {
				expect(socketIoResponse.status).to.equal(true);
				expect(socketIoResponse.socket).to.exist;
				expect(socketIoResponse.socket.connected).to.equal(true);
				
				let roomId;
				cy.get('@newRoomId').then(roomIdVal => {
					cy.emitEvent(socketIoResponse.socket, {
						eventName: 'startGame',
						eventParams: {
							roomId: roomIdVal,
							gameId: 1 
						}
					}).then((startGameresp) => {
						expect(startGameresp?.status).to.be.true;
					});

					cy.emitEvent(socketIoResponse.socket, {
						eventName: 'startGame',
						eventParams: {
							roomId: roomIdVal,
							gameId: 1 
						}
					}).then((startGameresp) => {
						expect(startGameresp?.status).to.be.false;
					});
				});
			});
		});
	});
});