// ***********************************************
// This example commands.js shows you how to
// create various custom commands and overwrite
// existing commands.
//
// For more comprehensive examples of custom
// commands please read more here:
// https://on.cypress.io/custom-commands
// ***********************************************
//
//
// -- This is a parent command --
// Cypress.Commands.add("login", (email, password) => { ... })
//
//
// -- This is a child command --
// Cypress.Commands.add("drag", { prevSubject: 'element'}, (subject, options) => { ... })
//
//
// -- This is a dual command --
// Cypress.Commands.add("dismiss", { prevSubject: 'optional'}, (subject, options) => { ... })
//
//
// -- This will overwrite an existing command --
// Cypress.Commands.overwrite("visit", (originalFn, url, options) => { ... })

const socketIOClient = require("socket.io-client");

Cypress.Commands.add('createSocketConnection', (serverUrl, {closeOnConnect, queryParams, failOnError = true} = {}) => {
	const responseObj = {status: true};

	const socket = socketIOClient.io(serverUrl, {
		reconnectionDelayMax: 10000,
		query: {
			...(queryParams ? queryParams : {})
		},
	});

	return new Cypress.Promise((resolve, reject) => {
		socket.on('connect', () => {
			setTimeout(() => {
				if (closeOnConnect) {
					socket.close();
				}

				responseObj.socket = socket; 
				
				resolve(responseObj);
			}, 500);
		});

		socket.on('error', (error) => {
			responseObj.status = false;
			responseObj.error = error;
			reject(responseObj);
		});

		socket.on('connect_error', () => {
    		socket.close();
    		responseObj.status = false;
    		responseObj.error = 'Connection Failed';
    		if (failOnError) {
    			reject(responseObj);	
    		} else {
    			resolve(responseObj);
    		}
		});

		socket.on('connectError', () => {
    		socket.close();
    		responseObj.status = false;
    		responseObj.error = 'Connection Failed';
    		if (failOnError) {
    			reject(responseObj);	
    		} else {
    			resolve(responseObj);
    		}
		});
	});
});


Cypress.Commands.add('createNewRoom', () => {
	const responseObj = {status: true};
	cy.request({
		method: "POST", 
		url: "localhost:8084/api/createNewRoom",
		body: {
			roomName: `${Date.now()}`,
			userName: "CypressUser"
		}
	}).then((createRoomResponse) => {
		return createRoomResponse;
	});
});

Cypress.Commands.add('emitEvent', (ioSocket, {eventName, eventParams = {}} = {}) => {
	expect(ioSocket).to.exist;
	expect(eventName).to.exist;
	expect(eventParams).to.exist;
	expect(ioSocket.connected).to.equal(true);
	
	return new Cypress.Promise((resolve, reject) => {
		let timeoutObj = setTimeout(() => {
			reject();
		}, 2000);

		ioSocket.on('gameStarted', () => {
			clearTimeout(timeoutObj);
			resolve({status: true});
		});

		ioSocket.on('startGameError', () => {
			clearTimeout(timeoutObj);
			resolve({status: false});
		});

		ioSocket.emit(eventName, eventParams);
	});
});


Cypress.Commands.add('startGameWithMultiSockets', (ioSockets, {eventName, eventParams = {}} = {}) => {
	
	expect(ioSockets).to.exist;
	expect(ioSockets.length).to.exist;
	expect(eventName).to.exist;
	expect(eventParams).to.exist;
	

	
	const totalSockets = ioSockets.length;
	const promiseArr = [];
	ioSockets.forEach((socket) => {
		expect(socket.connected).to.equal(true);
		promiseArr.push(new Promise((resolve, reject) => {
			let timeoutObj = setTimeout(() => {
				reject({status: false});
			}, 1000);

			ioSocket.on('gameStarted', () => {
				clearTimeout(timeoutObj);
				resolve({status: true});
			});

			ioSocket.on('startGameError', () => {
				clearTimeout(timeoutObj);
				resolve({status: false});
			});
		}));	
	});
	
	
	return new Cypress.Promise((resolve, reject) => {
		Promise.allSettled(promiseArr).then((results) => {
			let failures = 0;
			results.forEach((result) => {
				if(result.status === 'rejected') {
					failures++;	
				}
			});

			if (failures) {
				resolve({status: true});
			} else {
				resolve({status: false});
			}
		});

		ioSockets[0].emit(eventName, eventParams);
	});
});
