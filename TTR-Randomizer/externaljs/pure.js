'use strict';

module.exports = {
    generateUuid: () => {
        let d = new Date().getTime();
	    if (typeof window !== 'undefined' && window.performance && typeof window.performance.now === 'function') {
            d += performance.now() // use high precision timer if available
	    }

        let uuid = 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function (c) {
            let r = (d + Math.random()*16)%16 | 0;
            d = Math.floor(d/16);
            return (c=='x' ? r : (r&0x3|0x8)).toString(16);
        });
        return uuid;
    },
    factorial: (target) => {
        if (target == 0) {
            return 1;
        }

        return target * module.exports.factorial(target-1);
    },
    // Commented By Akash Mehta
    // Summation of first n natural numbers = 1/2 * n * (n+1) 
    // Hence in our case the first n-1 natural numbers will be = 1/2 * (n-1) * (n-1 + 1) = 1/2 * (n-1) * n
    maxAvailableTickets: (totalNodes) => {
        if (totalNodes <= 0) {
            return 0;
        }
        
        return (totalNodes - 1) * (totalNodes) * (0.5);
    }

};
