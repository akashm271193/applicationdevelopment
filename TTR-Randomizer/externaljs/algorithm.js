'use strict';

var _ = require('lodash');
var Path = require('./classes/path.js');
var Node = require('./classes/node.js');
var Graph = require('./classes/graph.js');
const Pure = require('./pure.js');

const Algorithm = function() {
	

	/***
		Commented By Akash Mehta on 25th July 2020
		This function creates a graph based on the input
		Expected input ::
		[{
			nodeId:,
			nodeName:,
			connections: [{
				nodeId:,
				distance: ,
				routes:
			}]
		}];
	***/
	var createGraph = function (nodes) {
		var graph = new Graph();
		graph.addNodes(nodes);
		graph.generatePaths();
		return graph;
	}


	var _findShortestPath = (nodesToVisitNextAsPerLevel, currentLevel, shortestPathsMap, unvisitedNodes, targetNodeId, sourceCityId, ticketKey) => {
		
		let nodesToVisitNext = nodesToVisitNextAsPerLevel[currentLevel] || [];

		const nextLevel = currentLevel + 1;

		nodesToVisitNext.forEach((currentNodeId) => {
			// If node already visited, then exit
			if (!unvisitedNodes[currentNodeId]) {
				return;
			}

			// console.log(`Current Node Id: ${currentNodeId}`);

			let currentNode = unvisitedNodes[currentNodeId];
			
			// Mark current Node as visited
			if(currentNodeId !== targetNodeId) {
				delete unvisitedNodes[currentNodeId];
			}

			const shortestPathToCurrentNode = shortestPathsMap[currentNodeId];

			// Maintain a list of nodes to visit next
			const newNodesToVisitNext = [];

			// Iterate over the connections of the current node and calculate their path relative to the starting point
			// If the new calculated path length is less than any of the existing discovered paths for the connection node,
			// then update the shortest path Map
			// Irrespective of the path length, we need to visit the all the connection nodes next only if the any connection node is not the target node
			for(const connection of Object.values(currentNode.getConnections)) {
				if (unvisitedNodes[connection.nodeId]) {
					let existingPathLength = shortestPathsMap[connection.nodeId] || 999999;
					let newPathLength = shortestPathToCurrentNode + connection.distance;
					shortestPathsMap[connection.nodeId] =  Math.min(existingPathLength, newPathLength);
					newNodesToVisitNext.push(connection.nodeId);
				}
			}

			let nextLevelNodes = nodesToVisitNextAsPerLevel[nextLevel] || [];
			nextLevelNodes.push(...newNodesToVisitNext);
			nodesToVisitNextAsPerLevel[nextLevel] = nextLevelNodes;
		});
		

		if (nodesToVisitNextAsPerLevel[nextLevel]?.length) {
			_findShortestPath(nodesToVisitNextAsPerLevel, nextLevel, shortestPathsMap, unvisitedNodes, targetNodeId, sourceCityId, ticketKey);
		}
	}


	var generateAllTickets = (gameBoard) => {
		const gameBoardCities = gameBoard.getAllNodes;
		const gameBoardCityIds = gameBoardCities.map((city) => {
			return city.nodeId;
		});

		const allTickets = {};

		for(const sourceCityId of gameBoardCityIds) {
		
			// console.log(`######### Iteration for sourceCityId: ${sourceCityId} #########`);	
			const shortestPathsMap = {};
			shortestPathsMap[sourceCityId] = 0;

			for(const targetCityId of gameBoardCityIds) {
				const nodesToVisitNextAsPerLevel = {
					0: [sourceCityId],
				};

				if (sourceCityId == targetCityId) {
					continue;
				}

				const minNodeId = Math.min(sourceCityId, targetCityId);
				const maxNodeId = Math.max(sourceCityId, targetCityId);
				const ticketKey = '' + minNodeId + '-' + maxNodeId;

				const  unvisitedNodes = gameBoardCities.reduce((map, city) => {
					map[city.nodeId] = city;
					return map;
				}, {});

				// console.log(`============ Iteration for targetNodeId: ${targetCityId} ============`);
				// console.log(`Ticket Key: ${ticketKey}`)
				 _findShortestPath(nodesToVisitNextAsPerLevel, 0, shortestPathsMap, unvisitedNodes, targetCityId, sourceCityId, ticketKey);

				// console.log(JSON.parse(JSON.stringify(shortestPathsMap)));
				if (shortestPathsMap[targetCityId] && (!allTickets[ticketKey] || shortestPathsMap[targetCityId] < allTickets[ticketKey])) {
					allTickets[ticketKey] =	shortestPathsMap[targetCityId];
				}

				// console.log(JSON.parse(JSON.stringify(allTickets)));
			}
		}


		return allTickets;
	}

	// Need to add sessionId
	var generateTickets = function (gameDetails, nodes) {
		let gameBoard = createGraph(nodes);
		let allAvailableTickets = generateAllTickets(gameBoard);


		gameDetails = gameDetails || {};
		gameDetails.sessionId = gameDetails.sessionId || "";
		gameDetails.numUsers = gameDetails.numUsers || 5;
		gameDetails.initialTicketsPerPlayer = gameDetails.initialTicketsPerPlayer || 4;

		// Code to ensure we are not issuing more tickets than are possible given
		// a particular graph
		const maxPossibleTickets = Object.keys(allAvailableTickets).length;
		const requestedTickets = gameDetails.totalTickets || 30;
		const totalTickets = Math.min(requestedTickets, maxPossibleTickets);
		
		const selectedTickets = _.sampleSize(Object.keys(allAvailableTickets), totalTickets);

		const finalIssuedTickets = selectedTickets.reduce((ticketsArr, ticketKey) => {
			const ticket = {};
			const sourceCityId = ticketKey.split('-')[0];
			const targetCityId = ticketKey.split('-')[1];
			const sourceCity = nodes.find((node) => node.nodeId === parseInt(sourceCityId))
			const targetCity = nodes.find((node) => node.nodeId === parseInt(targetCityId))

			if (!sourceCity || !targetCity) {
				throw new Error('Error while find source or target city');
			}

			ticket.sourceCityId = sourceCityId;
			ticket.destinationCityId = targetCityId;
			ticket.sourceCity  = sourceCity.nodeName;
			ticket.destinationCity  = targetCity.nodeName;
			ticket.score = allAvailableTickets[ticketKey];
			ticketsArr.push(ticket);
			return ticketsArr;
		}, []);

		gameDetails.totalTickets = finalIssuedTickets.length;
		gameDetails.tickets = _.cloneDeep(finalIssuedTickets);

		let playerMaps = {};
		for(let playerIndex = 0; playerIndex < gameDetails.numUsers; playerIndex++) {
			playerMaps[playerIndex] = finalIssuedTickets.splice(0, gameDetails.initialTicketsPerPlayer);
		};


		gameDetails.currentStack = finalIssuedTickets;
		gameDetails.playerTickets = playerMaps;

		return gameDetails;
	}

	return {
		createGraph: createGraph,
		generateAllTickets: generateAllTickets,
		generateTickets,
	}	
};

if (typeof module !== 'undefined' && typeof module.exports !== 'undefined') {
    module.exports = new Algorithm();
} else {
    window.Algorithm = new Algorithm();
}