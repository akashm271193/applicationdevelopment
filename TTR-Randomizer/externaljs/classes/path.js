'use strict';

class Path {	
	constructor(nodeId1 = 0, nodeId2 = 0, distance = 0, routes = 0) {
		try {
			var tmpNodeId1 = parseInt(nodeId1); 
			var tmpNodeId2 = parseInt(nodeId2);
			var tmpDist = parseInt(distance); 
			var tmpRoute = parseInt(routes);
			
			if (!tmpNodeId1 || !tmpNodeId2 || (tmpNodeId1 == tmpNodeId2) || !tmpDist || !tmpRoute || tmpDist < 0 || tmpRoute < 0) {
				throw 'exception';			
			}
			
			// Based on the ordering of ids
			// assign the respective nodes 
			// for unique path generation
			if (tmpNodeId1 - tmpNodeId2 < 0) {
				this.nodeId1 = tmpNodeId1;
				this.nodeId2 = tmpNodeId2;
			} else {
				this.nodeId1 = tmpNodeId2;
				this.nodeId2 = tmpNodeId1;
			}

			this.distance = tmpDist;
			this.routes = tmpRoute;
		} catch (ex) {
			throw {
				error : 'Invalid parameters for new Path - ' + JSON.stringify({
					nodeId1 : nodeId1,
					nodeId2 : nodeId2,
					distance : distance,
					routes : routes					
				})
			};
		}

	}


	get generateUniquePathId() {
		return '' + this.nodeId1 + '-' + this.nodeId2 + '-' + this.distance;
	}
}

if (typeof module !== 'undefined' && typeof module.exports !== 'undefined') {
    module.exports = Path;
} else {
    window.Path = Path;
}