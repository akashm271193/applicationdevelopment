'use strict';

var Node = require('./node.js');
var Path = require('./path.js');

class Graph {
	constructor() {
		this._allNodes = [];
		this._allPaths = {};
	};

	_addNode(inputNode = {}){
		if (!inputNode || !inputNode.nodeId || !inputNode.nodeName) {
			throw { 
				error : 'Invalid Input - ' + JSON.stringify(inputNode)
			};
		}

		var newNode = new Node(inputNode.nodeId, inputNode.nodeName);
		this._allNodes.push(newNode);
	};

	_addConnections(inputNode = {}) {
		var node = this._allNodes.find((_node) => _node.nodeId == inputNode.nodeId);
		if (node) {
			node.addConnections(inputNode.connections);
		}
	};

	addNodes(nodes = []){
		nodes.forEach((inputNode) => {
			this._addNode(inputNode);
		});

		nodes.forEach((inputNode) => {
			this._addConnections(inputNode)
		});
	};

	_addPath(path = {}){
		if (!path || !path.nodeId1 || !path.nodeId2 || !path.distance || !path.routes) {
			throw {
				error : 'Invalid Path - ' + JSON.stringify(path)
			};		
		}
		
		var pathId = path.generateUniquePathId;
		this._allPaths[pathId] = path;
	};

	get getAllNodes() {
		return this._allNodes;
	};

	get getAllPaths() {
		return this._allPaths;
	};

	generatePaths() {
		this._allNodes.forEach((node) => {
			if (node.connectedNodes) {
				Object.keys(node.connectedNodes).forEach((connectionNodeKey) => {
					var connection = node.connectedNodes[connectionNodeKey];
					var path = new Path(node.nodeId, connection.nodeId, connection.distance, connection.routes);
					this._addPath(path);
				});
			}
		});
	};
}


if (typeof module !== 'undefined' && typeof module.exports !== 'undefined') {
    module.exports = Graph;
} else {
    window.Graph = Graph;
}