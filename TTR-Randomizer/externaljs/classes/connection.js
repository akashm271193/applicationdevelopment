'use strict';

// Commented By Akash Mehta on August 8 2021
// This class represents a connection object which holds the details
// of all connected to nodes to the current node
// Two nodes can be connected to each other with multiple routes
// NodeId and Distance is the unique identifier as you can have multiple routes of the same distance
// or you can have multiple routes of different distances between nodes
class Connection {	
	constructor(nodeId, distance, routes) {
		try {
			var tmpNodeId = parseInt(nodeId);
			var tmpDist = parseInt(distance); 
			var tmpRoute = parseInt(routes);
			if (!tmpNodeId || !tmpDist || !tmpRoute || tmpDist < 0 || tmpRoute < 0) {
				throw 'exception';
			}

			this.nodeId = tmpNodeId;
			this.distance = tmpDist;
			this.routes = tmpRoute;
		} catch (ex) {
			throw {
				error : 'Invalid parameters for new Connection - ' + JSON.stringify({
					nodeId: nodeId,
					distance : distance,
					routes : routes					
				})
			};
		}
	}

	get generateUniqueConnectionId() {
		return '' + this.nodeId + '-' + this.distance;
	}
}

if (typeof module !== 'undefined' && typeof module.exports !== 'undefined') {
    module.exports = Connection;
} else {
    window.Connection = Connection;
}