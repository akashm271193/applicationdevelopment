'use strict';

var Connection = require('./connection.js');

class Node {
	constructor(nodeId, nodeName) {
		try{
			var tmpNodeId = parseInt(nodeId);
			if (!tmpNodeId || !nodeName) {
				throw 'exception';	
			}
			
			this.nodeId = tmpNodeId;
			this.nodeName = nodeName;
			this.connectedNodes = {};	
		} catch (ex) {
			throw {
				error : 'Invalid parameters for new Node - ' + JSON.stringify({
					nodeId1 : nodeId,
					nodeName : nodeName
				})
			};
		}
	}

	addConnections(connectionsArr = []) {
		if (connectionsArr) {
			connectionsArr.forEach((input) => {
				const newConnection = new Connection(input.nodeId, input.distance, input.routes)
				var uniqueKey =  newConnection.generateUniqueConnectionId;
				this.connectedNodes[uniqueKey] = newConnection;
			});
		}
	}

	get getConnections() {
		return this.connectedNodes || {};
	}
}

if (typeof module !== 'undefined' && typeof module.exports !== 'undefined') {
    module.exports = Node;
} else {
    window.Node = Node;
}