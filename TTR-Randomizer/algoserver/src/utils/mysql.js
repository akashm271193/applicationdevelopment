const mysql = require('mysql2');

// Create the connection pool. The pool-specific settings are the defaults
let mysqlPool;

const MysqlUtil = (appConfig) => {
	const _init = async () => {
		mysqlPool = await mysql.createPool({
			host: appConfig.db.host,
			user: appConfig.db.username,
			database: appConfig.db.database,
			password: appConfig.db.password,
			waitForConnections: true,
			connectionLimit: 10,
			queueLimit: 0
		});
	};

	var getConnection = () => {
		try {
			return new Promise((resolve, reject) => {
				mysqlPool.getConnection(function(err, conn)  {
					if (err) {
						reject(err);
					}

					resolve(conn);
				});
			});
		} catch (error) {
			console.log(error);
			return Promise.reject(error);
		}
	};

	var closeConnection = (connection) => {
		try {
			return new Promise((resolve, reject) => {
				mysqlPool.releaseConnection(connection);
				resolve();
			});
		} catch (error) {
			console.log(error);
			return Promise.reject(error);
		}
	};

	_init();
	
	return {
		getConnection,
		closeConnection,
	};
}


/*if (typeof module !== 'undefined' && typeof module.exports !== 'undefined') {
    module.exports = new MysqlUtil();
} else {
    window.MysqlUtil = new MysqlUtil();
}*/

module.exports = MysqlUtil;