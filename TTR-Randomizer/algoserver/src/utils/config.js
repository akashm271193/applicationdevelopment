'use strict';
var path = require('path');
const fs = require('fs');

let rawConfig = fs.readFileSync(path.join(__dirname, '../config.json'));
let config = JSON.parse(rawConfig);

module.exports = config;
