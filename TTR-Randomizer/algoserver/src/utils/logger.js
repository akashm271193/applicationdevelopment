var winston = require('winston');
var path = require('path');
require('winston-daily-rotate-file');
var AppConfig = require('./config.js');
var fs = require('fs');

var logPath = AppConfig.logPath || path.join(AppConfig.appRootPath, 'logs');
var applicationLogDirectory = path.join(logPath, 'application');
var accessLogDirectory = path.join(logPath, 'access');

// Ensure log directory exists
fs.existsSync(applicationLogDirectory) || makeDirectory(applicationLogDirectory);
fs.existsSync(accessLogDirectory) || makeDirectory(accessLogDirectory);

console.log('Application Log File Path : ' + applicationLogDirectory);
console.log('Access Log File Path : ' + accessLogDirectory);

var applicationLogTransport = new (winston.transports.DailyRotateFile)({
    filename: 'application.log-%DATE%',
    datePattern: 'YYYYMMDD',
    dirname: applicationLogDirectory,
    zippedArchive: true,
    maxFiles: '14d'
});

var accessLogTransport = new (winston.transports.DailyRotateFile)({
    filename: 'access.log-%DATE%',
    datePattern: 'YYYYMMDD',
    dirname: accessLogDirectory,
    zippedArchive: true,
    maxFiles: '5d'
});

var applicationLogger = new winston.createLogger({
  transports: [
    applicationLogTransport
  ],
  exitOnError: false, // do not exit on handled exceptions
});

var accessLogger = new winston.createLogger({
  transports: [
    accessLogTransport
  ],
  exitOnError: false, // do not exit on handled exceptions
});

applicationLogger.logInfo = function (uniqueId = new Date().now(), data) {
  applicationLogger.info({
      'uniqueId': uniqueId,
      'data': data
  });
};

applicationLogger.logError = function (data) {
  applicationLogger.error(data);
};

accessLogger.stream = {
  write: function (message, encoding) {
    accessLogger.info(message);
  },
};

function returnLogger (type) {
  if (type && type == 'access') {
    return accessLogger;
  }
  return applicationLogger;
}

/**
*** Commented By Akash Mehta on 27th May 2020
*** We need to do the below step as the inbuilt
*** fs functionality to handle recursive directory
*** is only available post node 10.0.
*** So here we iterate through the entire path and ensure
*** That every folder exists and if not , we create it
***/
function makeDirectory (path) {
  var paths = path.split('/');
  var directoryPath = '';
  for (var cntr = 0; cntr < paths.length; cntr++) {
      directoryPath += paths[cntr] + '/';
      fs.existsSync(directoryPath) || fs.mkdirSync(directoryPath);
  }
}

module.exports = returnLogger;
