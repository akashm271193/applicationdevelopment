const redis = require('redis');

// Create the connection pool. The pool-specific settings are the defaults
let redisClient;

const globalLockTimeoutInSeconds = 5;


const RedisUtil = (appConfig) => {
	const _init = async () => {
		console.log("Waiting for redis initialization");

		console.log("Creating client");
		redisClient = redis.createClient({
			url: appConfig.redis.url
		});

		console.log('Client created')

		redisClient.on('error', (err) => { 
			console.log(`Unable to connect to database: ${err.stack}`);
		});

		await redisClient.connect().catch((err) => {
			console.log(err);
		});
		
		console.log("Connected Successfully");
	}

	const _initializeRedisClient = () => {};

	var getValue = (redisKey) => {
		return new Promise(async (resolve, reject) => {
			try {
				if (!redisClient) {
					// To LOG
					reject('No client found! Rejecting getKey request');
				}

				const redisValue = await redisClient.get(redisKey);
				resolve(redisValue);
			} catch (error) {
				reject(`Failed to get redisKey: ${redisKey}  with error ${error.stack}`)
			}
		});
	};

	var setValue = (redisKey, redisValue, {expiryInSeconds} = {}) => {
		return new Promise(async (resolve, reject) => {
			try {
				if (!redisClient) {
					// To LOG
					reject('No client found! Rejecting getKey request');
				}

				let additionalParams = {};
			
				if (expiryInSeconds) {
					additionalParams.EX = expiryInSeconds;
				}
				await redisClient.set(redisKey, redisValue, additionalParams);

				resolve();
			} catch (error) {
				reject(`Failed to set redisKey: ${redisKey} with error ${error.stack}`)
			}
		});
	};

	var deleteKey = (redisKey) => {
		return new Promise(async (resolve, reject) => {
			try {
				if (!redisClient) {
					// To LOG
					reject('No client found! Rejecting getKey request');
				}

				await redisClient.del(redisKey);
				resolve();
			} catch (error) {
				reject(`Failed to delete redisKey: ${redisKey} with error ${error.stack}`)
			}
		});
	};


	const acquireLock = (redisKey, redisValue, lockTimeOutInSeconds = 5000) => {
		return new Promise(async (resolve, reject) => {
			try {
				const immediateObj = setImmediate(() => {

					if (!redisClient) {
						// To LOG
						reject('No client found! Rejecting getKey request');
						clear(immediateObj);
					}

					redisClient.set(redisKey, redisValue, {
						NX: true,
						// EX: 15,

					}).then(() => {
						if (timeOutObj) {
							clearTimeout(timeOutObj);
						}

						if (immediateObj) {
							clearImmediate(immediateObj);
						}
						resolve();
					}).catch((error) => {
						console.log(`Sleeping for 1 sec! Unable to acquire lock : ${error.stack} `);
						return;
					});
				}, 1000);

				const timeOutObj = setTimeout(() => {
					if (immediateObj) {
						clearImmediate(immediateObj);
					}

					reject('Unable to acquire Lock');
				}, lockTimeOutInSeconds);
			} catch (error) {
				reject(`Failed to acquire lock for redisKey: ${redisKey} with error ${error.stack}`)
			}
		});
	};	

	_init();
	
	return {
		getValue,
		setValue,
		deleteKey,
		acquireLock
	};
}


/*if (typeof module !== 'undefined' && typeof module.exports !== 'undefined') {
    module.exports = new RedisUtil();
} else {
    window.RedisUtil = new RedisUtil();
}*/

module.exports = RedisUtil;