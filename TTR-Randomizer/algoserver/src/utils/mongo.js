const mongoose = require('mongoose');

const MongoUtil = (appConfig) => {
	const _init = async () => {
		await mongoose.connect(appConfig.mongo.url);
	};

	_init();
	
	return {
		mongoose,
	};
}


/*if (typeof module !== 'undefined' && typeof module.exports !== 'undefined') {
    module.exports = new MysqlUtil();
} else {
    window.MysqlUtil = new MysqlUtil();
}*/

module.exports = MongoUtil;