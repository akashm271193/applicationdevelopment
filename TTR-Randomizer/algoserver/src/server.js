'use strict';

var bodyParser = require('body-parser');
var express = require('express');
var morgan = require('morgan');
var app = express();

var logger = require('./utils/logger')('access');

require('source-map-support').install();


var port = process.env.PORT || 8084;

app.use(morgan('combined', {stream: logger.stream})); // setup the logger
app.use(bodyParser.json()); // get information from html forms
app.use(bodyParser.urlencoded({extended: false}));

/*var whitelist = ['localhost'];
allowRequest: (req, callback) => {
		try {
			console.log(req?.headers?.origin);
      	    const origin = new URL(req?.headers?.origin);
      	    console.log(origin.hostname);

      	    if (whitelist.indexOf(origin.hostname) !== -1) {
	          callback(null, true);
	   	    } else {
	   	    	console.log(origin);
	        	callback(new Error('Not allowed by CORS'))
	        }
		} catch (ex) {
			console.log(ex);
			callback(new Error('Oops! Something went wrong while processing your request'));
		}
}*/


const server = require('http').createServer(app);
const socketIoServer = require('socket.io')(server, {
	cors: {
		// TODO: Figure out a way to handle CORS
    	origin: 'http://localhost:41535',
    	methods: ["GET", "POST"]
  	}
});
server.listen(3000);


// Registering config
var appConfig = require('./utils/config.js');

// Var load external files
const ttrAlgorithm = require('./../../externaljs/algorithm.js');

// Register Utils
const loggerUtil = require('./utils/logger')();
const mysqlUtil = require('./utils/mysql')(appConfig);
const redisUtil = require('./utils/redis')(appConfig);
const sharedUtil = require('./utils/shared')();
const {mongoose: mongooseObj} = require('./utils/mongo')(appConfig);

// Register models
const ttrGameDataModel = require('./models/ttrGameData.model.js')(mongooseObj);

// Register Controllers
const roomController = require('./controllers/room.controller.js')(mysqlUtil, loggerUtil, redisUtil, sharedUtil);
const gameController = require('./controllers/game.controller.js')(socketIoServer, redisUtil, mysqlUtil, loggerUtil, ttrGameDataModel, ttrAlgorithm);


require('./api.js')(app, roomController, gameController);

app.listen(port);
