var TtrGameDataModel = (mongooseObj) => {
	// const Schema = mongooseObj.Schema;
	const ObjectId = mongooseObj.Schema.ObjectId;

	const ticketSubSchema = mongooseObj.Schema({
		sourceCityId: {type: String},
	    destinationCityId: {type: String},
	    sourceCity: {type: String},
	    destinationCity: {type: String},
	    score: {type: Number},
	}, {
		_id : false 
	});

	const playerSubSchema = mongooseObj.Schema({
		userId: {type: String},
		tickets: [ticketSubSchema]
	}, {
		_id : false 
	});

	const roomGame = mongooseObj.Schema({
		sessionId: {type: String},
		numUsers: {type: Number},
		totalTickets: {type: Number},
		initialTicketsPerPlayer: {type: Number},
		tickets: [ticketSubSchema],
		currentStack: [ticketSubSchema],
		playerState: [playerSubSchema]
	});

	return mongooseObj.model('ttrGameData', roomGame);
};


module.exports = TtrGameDataModel;