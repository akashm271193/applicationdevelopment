const GameController = (socketIoServer, redisUtil, mysqlUtil, loggerUtil, ttrGameDataModel, ttrAlgorithm) => {

	const _onStartGame = async (socket, gameData) => {
		let uniqueId = new Date().getTime() + '' + Math.floor(Math.random() * Math.floor(100000));
        let errorObj = {
            uniqueId: uniqueId
        };

        const roomId = gameData?.roomId;
    	const gameId = gameData?.gameId;
    	let mysqlConnection;
        try {
        	loggerUtil.logInfo(uniqueId, {
                message: 'Request recieved for starting a new game',
                data: gameData
            });

            if (!roomId || !gameId) {
                errorObj.error = 'Invalid parameters!';
                errorObj.data = gameData;
                loggerUtil.logError(errorObj);
                socket.emit('error', errorObj);
            }

            // Replace value with user session id 
            // To track who has lock
            await redisUtil.acquireLock(`lock:${roomId}`, `true`);

			mysqlConnection = await mysqlUtil.getConnection(); 
        } catch (error) {
        	const newErrorObj = JSON.parse(JSON.stringify(errorObj));
        	newErrorObj.data = 'Unexpected error while creating a new room';
            newErrorObj.error = JSON.stringify(error.stack);
            loggerUtil.logError(newErrorObj);

            errorObj.error = 'We are unable to process this request at the moment! Please try again later'
            socket.emit('startGameError', errorObj);
            // Need to figure out a better way rather than returning
            return;
        }

		
        try {
			await mysqlConnection.promise().query("START TRANSACTION");

			let [rows, fields] = await mysqlConnection.promise().query('Select * from rooms where id = ?', [roomId]);            
			let invalidParams = false, isValidRequest = true;

			if (!rows?.length) {
				// Code to log the results to the log file
				const newErrorObj = JSON.parse(JSON.stringify(errorObj));
				newErrorObj.error = `No rooms found for roomId : ${roomId}!`
				newErrorObj.data = rows;
				loggerUtil.logError(newErrorObj);
				
				invalidParams = true;
			}

			if (!invalidParams) {
				[rows, fields] = await mysqlConnection.promise().query('Select * from games where id = ?', [gameId]);
				if (!rows?.length) {
					// Code to log the results to the log file
					const newErrorObj = JSON.parse(JSON.stringify(errorObj));
					newErrorObj.error = `No games found for gameId: ${gameId}!`
					newErrorObj.data = rows;
					loggerUtil.logError(newErrorObj);
					
					invalidParams = true;
				}
			}

			if (invalidParams) {
				socket.emit(startGameError, 'Invalid Parameters!');
				return;
			} else {
				[rows, fields] = await mysqlConnection.promise().query('Select * from room_games where room_id = ? AND game_id = ? AND deleted_at IS NULL', [roomId, gameId]);
				if (rows?.length) {
					// Code to log the results to the log file
					const newErrorObj = JSON.parse(JSON.stringify(errorObj));
					newErrorObj.error = `Existing game found for the room. Cannot create a new one found for gameId: ${gameId}!`
					newErrorObj.data = rows;
					loggerUtil.logError(newErrorObj);
					isValidRequest = false;
					socket.emit(`startGameError`, 'Game already exists!');
					return;
				}


				if (isValidRequest) {
					loggerUtil.logInfo(uniqueId, {
		            	message: `No existing room_games found for Room id : ${roomId} and Game Id: ${gameId}. Creating a new room game!`
		            });

					const roomGameData = new ttrGameDataModel();
					await roomGameData.save();

					console.log(`Mongo id : ${roomGameData._id}`);

					const mongoRoomGameIdStr = `${roomGameData._id}`;

					await mysqlConnection.promise().query('INSERT INTO room_games(room_id, game_id, game_data_id) VALUES (?, ?, ?)', [roomId, gameId, mongoRoomGameIdStr]);

					// TO DO
					[rows, fields] = await mysqlConnection.promise().query(`Select * FROM 
						(
							(
								Select tm1.id as city1Id, tm1.city_name as city1, tm2.id as city2Id, tm2.city_name as city2, t1.distance, t1.routes 
								FROM ticket_to_ride t1 
								JOIN ticket_to_ride_meta tm1 ON t1.source_city_id = tm1.id AND tm1.game_id = ?
								JOIN ticket_to_ride_meta tm2 ON t1.target_city_id = tm2.id AND tm2.game_id = ?
							) UNION (
								Select tm1.id as city1Id, tm1.city_name as city1, tm2.id as city2Id, tm2.city_name as city2, t1.distance, t1.routes 
								FROM ticket_to_ride t1 
								JOIN ticket_to_ride_meta tm1 ON t1.target_city_id = tm1.id AND tm1.game_id = ?
								JOIN ticket_to_ride_meta tm2 ON t1.source_city_id = tm2.id AND tm2.game_id = ?
							)
						) as C ORDER BY C.city1Id;`, [gameId, gameId, gameId, gameId]);
					
					if (!rows?.length) {
						// TODO: throw error
					}

					const gameNodes = rows.reduce((nodesMap, route) => {
						const cityNode = nodesMap[route.city1Id] || {
							nodeId: route.city1Id,
							nodeName: route.city1,
							connections: [],
						};

						cityNode.connections.push({
							nodeId: route.city2Id,
							distance: route.distance,
							routes: route.routes,
						});

						nodesMap[route.city1Id] = cityNode;

						return nodesMap;
					}, {});

					const gameDetails = {
						sessionId: 'ABCDEF',
						numUsers: 3,
						totalTickets: 30,
					}

					const finalBoard = ttrAlgorithm.generateTickets(gameDetails, Object.values(gameNodes));
					roomGameData.sessionId = finalBoard.sessionId;
					roomGameData.numUsers = finalBoard.numUsers;
					roomGameData.totalTickets = finalBoard.totalTickets;
					roomGameData.initialTicketsPerPlayer = finalBoard.initialTicketsPerPlayer;
					roomGameData.currentStack = finalBoard.currentStack;
					roomGameData.tickets = finalBoard.tickets;

					console.log(roomGameData);

					// TODO: Add logic for populating player states
					// 
					await roomGameData.save();

					// Get users associate with room and add them to mongo
					// Code to create game data and update mongo
					// Code to create game data and update mongo
					// Code to create game data and update mongo
		            // TO DO
	        	}
			}

            await mysqlConnection.promise().query("COMMIT");
            await mysqlUtil.closeConnection(mysqlConnection);
            await redisUtil.deleteKey(`lock:${roomId}`);
            socketIoServer.to(`${roomId}`).emit('gameStarted', "Game has started. Fetch your tickets!");
        } catch (error) {
        	await mysqlConnection.promise().query("ROLLBACK");
        	await mysqlUtil.closeConnection(mysqlConnection);
        	await redisUtil.deleteKey(`lock:${roomId}`);
        	const newErrorObj = JSON.parse(JSON.stringify(errorObj));
        	newErrorObj.data = 'Unexpected error while starting a new game';
            newErrorObj.error = error?.stack ? JSON.stringify(error?.stack) : error;
            loggerUtil.logError(newErrorObj);

            errorObj.error = 'We are unable to process this request at the moment! Please try again later'
            socket.emit(`startGameError`, errorObj);
            return;
        }
	}

	const _onSocketIoConnection = async (socket) => {
		console.log(`IO CoNNECTED with id : ${socket.id}`);
        
        socket.on('disconnect', () => {
            console.log(`Socket id ${socket.id} disconnected! `);    
        });

        socket.on('startGame', (gameData) => {
        	_onStartGame(socket, gameData);
	    });

        const reqBody = socket.handshake?.query;
        const roomId = reqBody?.roomId;
        const magicLink = reqBody?.magicLink;
        const userUuid = reqBody?.userUuid;

        if (!roomId || !magicLink || !userUuid) {
            console.log("Invalid parameters");
            socket.emit("connectError", "Invalid Parameters");
            socket.disconnect(true);
            return;
        }

        const redisKey = `magicLink:${roomId}:${magicLink}`;

        const redisValue = await redisUtil.getValue(redisKey);
        if (!redisValue) {
            console.log("Invalid Magic Link");
            socket.emit("connectError", "Invalid Magic Link");
            socket.disconnect(true);
            return;
        }

        await redisUtil.deleteKey(redisKey);

        socket.join(`${roomId}`);

        // Settings two keys to ensure both mappings are always present
        await redisUtil.setValue(`socket:${socket.id}`, userUuid);
        await redisUtil.setValue(`user:${userUuid}`, `${socket.id}`);

        // TODO: Add redis map & mysql update for user and socket
        // await redisUtil.setValue(`${roomId}`, socket.id);

        socket.to(`${roomId}`).emit('newPlayerJoined', "Hello, I have joined!");

        console.log(socketIoServer.in(`${roomId}`).allSockets());
	};


	console.log("Registering Server");
    socketIoServer.on('connection', (socket) => {
		_onSocketIoConnection(socket);
    });
}



module.exports = GameController;