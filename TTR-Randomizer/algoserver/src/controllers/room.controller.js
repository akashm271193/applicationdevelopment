const RoomController = function (mysqlUtil, loggerUtil, redisUtil, sharedUtil) {
	const createNewRoom = async (req, res) => {
        let uniqueId = new Date().getTime() + '' + Math.floor(Math.random() * Math.floor(100000));
        let errorObj = {
            uniqueId: uniqueId
        };

        let mysqlConnection;
        try {
			mysqlConnection = await mysqlUtil.getConnection(); 
        } catch (error) {
        	const newErrorObj = JSON.parse(JSON.stringify(errorObj));
        	newErrorObj.data = 'Unexpected error while creating a new room';
            newErrorObj.error = JSON.stringify(error.stack);
            loggerUtil.logError(newErrorObj);

            errorObj.error = 'We are unable to process this request at the moment! Please try again later'
            return res.status(500).send(errorObj);
        }

		
        try {
			await mysqlConnection.promise().query("START TRANSACTION");
            
            var roomDetails = req.body;

            loggerUtil.logInfo(uniqueId, {
                message: 'Request recieved for creating a new room',
                data: roomDetails
            });

            if (!roomDetails?.roomName || !roomDetails?.userName) {
                errorObj.error = 'Invalid parameters!';
                errorObj.data = roomDetails;
                loggerUtil.logError(errorObj);
                return res.status(400).json(errorObj);
            }


            roomDetails.roomFid = roomDetails.roomName.toUpperCase().replace(/[^A-Z0-9\-]/g, "");

            loggerUtil.logInfo(uniqueId, {
            	message: `Room FID : ${roomDetails.roomFid} - generated for room: ${roomDetails.roomName}`
            });

			let [rows, fields] = await mysqlConnection.promise().query('Select * from rooms where room_fid = ?', [roomDetails.roomFid]);            
			if (rows?.length) {
				// Code to log the results to the log file
				const newErrorObj = JSON.parse(JSON.stringify(errorObj));
				newErrorObj.data = rows;
				loggerUtil.logError(newErrorObj);
				
				errorObj.error = 'Room already exists! Please try joining the room with given pin else create a new one.';
				
				// Always return the error back without any data from the db
				return res.status(400).json(errorObj);
			}

			loggerUtil.logInfo(uniqueId, {
            	message: `No existing rooms found for Room FID : ${roomDetails.roomFid} . Creating a new room!`
            });

            const pin = Math.floor(100000 + Math.random() * 900000) + '';
			
            // TODO:
            // Change pin field to password
            // This will be a mandatory user input field
            roomDetails.pin = pin;

			// function makeid(length) {
			// let externalRoomId = '';
			// let length = 6;
			// let characters = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789';
		    // var charactersLength = characters.length;
		    // for ( var i = 0; i < length; i++ ) {
		    //   externalRoomId += characters.charAt(Math.floor(Math.random() * charactersLength));
		 	// }
			
            await mysqlConnection.promise().query('INSERT INTO rooms (room_name, room_fid, pin) VALUES (?, ?, ?) ', [roomDetails.roomName, roomDetails.roomFid, pin]);

            [rows, fields] = await mysqlConnection.promise().query('Select id FROM rooms WHERE room_fid = ?', [roomDetails.roomFid]);

            if (!rows?.length) {
            	// Throw an error to be caught by catch block. There is no reason why a room should not pe present
            	// if the insert statement goes through as expected
				throw new Error(`Unable to create room with roomDetails : ${roomDetails}`);
            }

            roomDetails.roomId = rows[0].id;

            roomDetails.userUuid = `${roomDetails.roomId}:${sharedUtil.generateUuid()}`;

            await mysqlConnection.promise().query('INSERT INTO room_users (room_id, user_name, user_uuid) VALUES (?, ?, ?)', [roomDetails.roomId, roomDetails.userName, roomDetails.userUuid]);

            var magicLink = sharedUtil.generateUuid();

            await redisUtil.setValue(`magicLink:${roomDetails.roomId}:${magicLink}`, "1", {
                expiryInSeconds: 10,
            });

            roomDetails.magicLink = magicLink;

            await mysqlConnection.promise().query("COMMIT");

            await mysqlUtil.closeConnection(mysqlConnection);

            return res.json(roomDetails);
        } catch (error) {
        	await mysqlConnection.promise().query("ROLLBACK");
        	await mysqlUtil.closeConnection(mysqlConnection);
        	const newErrorObj = JSON.parse(JSON.stringify(errorObj));
        	newErrorObj.data = 'Unexpected error while creating a new room';
            newErrorObj.error = error?.stack ? JSON.stringify(error?.stack) : error;
            loggerUtil.logError(newErrorObj);

            errorObj.error = 'We are unable to process this request at the moment! Please try again later'
            return res.status(500).send(errorObj);
        }
	};


	const getAvailableRooms = async (req, res) => {
		let uniqueId = new Date().getTime() + '' + Math.floor(Math.random() * Math.floor(100000));
        var errorObj = {
            uniqueId: uniqueId
        };


        let mysqlConnection;
        try {
			mysqlConnection = await mysqlUtil.getConnection(); 
        } catch (error) {
        	const newErrorObj = JSON.parse(JSON.stringify(errorObj));
        	newErrorObj.data = 'Unexpected error while getting all available rooms';
            newErrorObj.error = JSON.stringify(error.stack);
            loggerUtil.logError(newErrorObj);

            errorObj.error = 'We are unable to process this request at the moment! Please try again later'
            return res.status(500).send(errorObj);
        }

        try {
            loggerUtil.logInfo(uniqueId, {
                message: 'Request recieved for getting all available rooms',
                data: req.headers
            });

			let [rows, fields] = await mysqlConnection.promise().query('Select * from rooms WHERE deleted_at IS NULL');
            
            
			// Ensuring rows is always present so we do not have to worry about
			// response being undefined
            rows = rows || [];

            const response = rows.map((roomDetails) => {
            	return {
            		roomId: roomDetails.id,
            		roomName: roomDetails.room_name,
            	}
            });

            await mysqlUtil.closeConnection(mysqlConnection);
	        return res.json(response);
        } catch (error) {
        	await mysqlUtil.closeConnection(mysqlConnection);
        	const newErrorObj = JSON.parse(JSON.stringify(errorObj));
        	newErrorObj.data = 'Unexpected error while getting all available rooms';
            newErrorObj.error = JSON.stringify(error.stack);
            loggerUtil.logError(newErrorObj);

            errorObj.error = 'We are unable to process this request at the moment! Please try again later'
            return res.status(500).send(errorObj);
        }
	};


	const joinRoom = async (req, res) => {
		let uniqueId = new Date().getTime() + '' + Math.floor(Math.random() * Math.floor(100000));
        var errorObj = {
            uniqueId: uniqueId
        };


        let mysqlConnection;
        try {
			mysqlConnection = await mysqlUtil.getConnection(); 
        } catch (error) {
        	const newErrorObj = JSON.parse(JSON.stringify(errorObj));
        	newErrorObj.data = 'Unexpected error while joining a room';
            newErrorObj.error = JSON.stringify(error.stack);
            loggerUtil.logError(newErrorObj);

            errorObj.error = 'We are unable to process this request at the moment! Please try again later'
            return res.status(500).send(errorObj);
        }

        try {
        	await mysqlConnection.promise().query("START TRANSACTION");
            
            var roomDetails = req.body;

            loggerUtil.logInfo(uniqueId, {
                message: 'Request recieved for joining a room',
                data: roomDetails
            });

            if (!roomDetails?.roomId || !roomDetails?.pin || !roomDetails?.userName) {
                errorObj.error = 'Invalid parameters!';
                errorObj.data = roomDetails;
                loggerUtil.logError(errorObj);
                return res.status(400).json(errorObj);
            }

			let [rows, fields] = await mysqlConnection.promise().query('Select * from rooms WHERE id = ? AND deleted_at IS NULL', [roomDetails.roomId]);

			let validationError = false;
			if (!rows?.length) {
				validationError = true;
				// Code to log the results to the log file
				const newErrorObj = JSON.parse(JSON.stringify(errorObj));
				newErrorObj.error = `No rooms found with the given room Id : ${roomDetails.roomId}`;
				loggerUtil.logError(newErrorObj);
			} 

			// Only check pin if we found a room
			if (!validationError && rows[0]?.pin !== `${roomDetails.pin}`) {
				validationError = true;
				// Code to log the results to the log file
				const newErrorObj = JSON.parse(JSON.stringify(errorObj));
				newErrorObj.error = `Invalid pin: ${roomDetails.pin} provided for room id: ${roomDetails.roomId}`
				loggerUtil.logError(newErrorObj);
			}

			if (validationError) {				
				errorObj.error = 'Invalid Room Details! Please select a valid room and provide a valid pin';
				// Always return the error back without any data from the db
				return res.status(400).json(errorObj);
			}

            roomDetails.userUuid = `${roomDetails.roomId}:${sharedUtil.generateUuid()}`;
			// TODO: Add idemptency check
			// TODO: Add idemptency check
			// TODO: Add idemptency check

			await mysqlConnection.promise().query('INSERT INTO room_users (room_id, user_name, user_uuid) VALUES (?, ?, ?)', [roomDetails.roomId, roomDetails.userName, roomDetails.userUuid]);


            var magicLink = sharedUtil.generateUuid();

            await redisUtil.setValue(`magicLink:${roomDetails.roomId}:${magicLink}`, "1", {
                expiryInSeconds: 10,
            });

            roomDetails.magicLink = magicLink;

            await mysqlConnection.promise().query("COMMIT");

            await mysqlUtil.closeConnection(mysqlConnection);

	        return res.json(roomDetails);
        } catch (error) {
        	await mysqlConnection.promise().query("ROLLBACK");
        	await mysqlUtil.closeConnection(mysqlConnection);

        	const newErrorObj = JSON.parse(JSON.stringify(errorObj));
        	newErrorObj.data = 'Unexpected error while joining a room';
            newErrorObj.error = JSON.stringify(error.stack);
            loggerUtil.logError(newErrorObj);

            errorObj.error = 'We are unable to process this request at the moment! Please try again later'
            return res.status(500).send(errorObj);
        }
	};
	
	return {
		createNewRoom,
		getAvailableRooms,
		joinRoom,
	};
}


module.exports = RoomController;