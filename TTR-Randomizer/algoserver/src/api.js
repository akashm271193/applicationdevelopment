'use strict';

const Algorithm = require('../../externaljs/algorithm.js');
const Logger = require('./utils/logger')();

module.exports = function (app, roomController, gameController) {
    var express = require('express');
    const lodash = require('lodash');
    const Decimal = require('decimal.js').default;
    var apiRoutes = express.Router();

    apiRoutes.get('/', function (req, res) {
        res.json({
            success: true,
            version: process.env.npm_package_version,
        });
    });
   
    apiRoutes.get(
        '/getAvailableRooms', 
        roomController.getAvailableRooms
    );

    apiRoutes.post(
        '/createNewRoom', 
        roomController.createNewRoom
    );

    apiRoutes.post(
        '/joinRoom', 
        roomController.joinRoom
    );

    /*function generateNownTransactionPayload (originalTransactionPayload, flatItemList, nownLocationId,
        adjustments, selectedNownTender, organizationSettings, menuPeriodId, fiitPatronId) {
        var nownPosData = NownCalculationService.createPosData(flatItemList, undefined, true, nownLocationId, {}, organizationSettings);

        var nownCalculatedObj = NownCalculationService.calculateBalances(flatItemList, [], adjustments, nownPosData);

        var nownTransactionObj = nownCalculatedObj.tenderAmounts;

        if (selectedNownTender) {
            var decimalAmount = new Decimal(selectedNownTender.amount || 0);
            var decimalAmountCents = decimalAmount.times(new Decimal(100));
            var amountCents = decimalAmountCents.toNearest(SharedDataService.baseCent).toNumber();
            var amountMeals = selectedNownTender.meal || 0;
            var decimalTipAmount = new Decimal(selectedNownTender.tipAmount || 0);
            var decimalTipAmountCents = decimalTipAmount.times(new Decimal(100));
            var tipAmountCents = decimalTipAmountCents.toNearest(SharedDataService.baseCent).toNumber();
            let tenderToAdd = selectedNownTender;
            var transactionTender = {
                transactionType: selectedNownTender.transactionType,
                order: 0,
                amountCents: amountCents,
                amountMeals: amountMeals,
                tipAmountCents: tipAmountCents,
                terminalResponse: tenderToAdd.terminalResponse,
                creditCardName: tenderToAdd.creditCardName,
                giftCardId: tenderToAdd.giftCardId,
                manuallyRecorded: tenderToAdd.manuallyRecorded
            };

            transactionTender.patronId = tenderToAdd.patronId;
            transactionTender.patronMealPlanId = tenderToAdd.patronMealPlanId;
            transactionTender.mealPlanId = tenderToAdd.mealPlanId;

            if (tenderToAdd.transactionTenderDetail) {
                transactionTender.transactionTenderDetail = tenderToAdd.transactionTenderDetail;
            }

            if (tenderToAdd.fiitTenders && tenderToAdd.fiitTenders.length) {
                transactionTender.fiitTenders = tenderToAdd.fiitTenders;
            }

            nownTransactionObj.tenders = [transactionTender];
        }

        nownTransactionObj.transactionUuid = originalTransactionPayload.transactionUuid;

        nownTransactionObj.lucovaUser = originalTransactionPayload.lucovaUser;

        nownTransactionObj.fiitPatronId = fiitPatronId;

        if (menuPeriodId) {
            nownTransactionObj.menuPeriodId = menuPeriodId;
        }

        nownTransactionObj.isMobileOrder = true;
        nownTransactionObj.autoCreateShift = true;
        nownTransactionObj.posStationId = originalTransactionPayload.posStationId || 0;

        return nownTransactionObj;
    }*/

    /**
    *** Commented By Akash Mehta on 1st April 2020
    *** This function creates a single tender object to be sent back in the response
    *** We geneate tenders based on whether there is a credit card transaction or not.
    *** Currently we only support two types of tenders for FIIT and only one type for Nown:
    *** 1. Credit Card (For both Nown & FIIT)
    *** 2. Fiit Meal Card (Single tender consisting of every other tender) (Only For FIIT)
    ***/
    /*async function generateNownTendersForPreorder (originalTransactionPayload, adjustments,
     modifiedFiitCalculationResult, fiitTransactionObj, isNownPreorder) {
        var currentTender = {};
        var isCreditCardTransaction = !!(originalTransactionPayload.creditCardAmount);
        if (isCreditCardTransaction) {
            currentTender = {
                type: 'card',
                field: 'creditCardAdjustment',
                amount: 0,
                transactionType: 'CREDIT'
            };
        } else {
            if (modifiedFiitCalculationResult && modifiedFiitCalculationResult.approvedAmount) {
                var fiitMpsTenderType = {
                    value: 'gateway',
                    name: 'Meal Card',
                    adjustment: 'otherAdjustment',
                    description: 'smb.pos.tenderType.gateway'
                };

                currentTender = {
                    type: fiitMpsTenderType.value,
                    field: fiitMpsTenderType.adjustment,
                    amount: 0,
                    transactionTenderDetail: {
                        otherType: 'fiitmps',
                        otherDisplayName: 'FIIT Meal Card'
                    },
                    transactionType: 'OTHER'
                };
            }
        }

        var adjustmentField = currentTender.field;

        if (!adjustmentField) {
            return;
        }

        var approvedAmount = new Decimal(0);

        if (isNownPreorder) {
            // The assumption here is that Nown will only be using credit card for now
            approvedAmount = new Decimal(originalTransactionPayload.creditCardAmount);
        } else {
            approvedAmount = new Decimal(modifiedFiitCalculationResult.approvedAmount);
        }

        var decimalCurrentAdjustmentAmount = new Decimal(currentTender.amount);
        var decimalOriginalAdjustmentAmount = new Decimal(adjustments[adjustmentField] || 0);
        var decimalUpdatedAdjustmentAmount = approvedAmount;
        var decimalNewAdjustmentAmount = decimalOriginalAdjustmentAmount
            .minus(decimalCurrentAdjustmentAmount)
            .plus(decimalUpdatedAdjustmentAmount);

        adjustments[adjustmentField] = decimalNewAdjustmentAmount.toNearest(SharedDataService.baseDollar).toNumber();

        currentTender.amount = decimalUpdatedAdjustmentAmount.toNumber();

        if (!isNownPreorder) {
            currentTender.meal = modifiedFiitCalculationResult.approvedMeal;
        }

        if (!isNownPreorder && !isCreditCardTransaction) {
            await FiitMealPlanCalculationService.updateSelectedNownTenderWithFiitTransactionObj(currentTender,
             modifiedFiitCalculationResult, fiitTransactionObj);
        }

        return currentTender;
    }*/

    app.use('/api', apiRoutes);
};
