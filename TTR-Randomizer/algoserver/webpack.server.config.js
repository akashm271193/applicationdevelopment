const path = require('path');
const webpack = require('webpack');

module.exports = (env, argv) => {

  return ({
    entry: {
      server: './src/server.js',
    },
    context: __dirname,
    output: {
      path: path.join(__dirname, 'dist'),
      publicPath: '/',
      filename: '[name].js',
    },
    mode: argv.mode,
    devtool: 'source-map',
    target: 'node',
    node: {
      // Need this when working with express, otherwise the build fails
      __dirname: false, // if you don't put this is, __dirname
      __filename: false, // and __filename return blank or /
    },
    resolve: {
      extensions: ['.ts', '.js'],
    },
    //  externals: [nodeExternals()], // Need this to avoid error when working with Express
    module: {
      rules: [
        {
          test: /\.ts$/,
          use: [
            'ts-loader',
          ]
        }
      ]
    }
  });
};
