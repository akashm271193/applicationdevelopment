import java.util.TreeMap;


/**
 * ENUM: eTypeOfNode
 * @author AK
 * @description: Enum representing Different types of node in a Trie
 * 
 */
enum eTypeOfNode{
	ROOT,CHILD,WORDEND;
}

/**
 * class: TrieNode
 * @author AK
 * @description: Structure class of a Node in a Trie; Consists of treemap mapping characters and their respective trienode &
 *  an enum indicating the type of node;
 */
public class TrieNode {
	private TreeMap<Character, TrieNode> childNodes;
	private eTypeOfNode nodeType;
	
	TrieNode(eTypeOfNode nodeType){
		childNodes = new TreeMap<>();
		this.nodeType = nodeType;
	}
	
	TrieNode(){
		childNodes = new TreeMap<>();
		this.nodeType = eTypeOfNode.CHILD;
	}
	
	public TreeMap<Character, TrieNode> getChildNodes() {
		return childNodes;
	}
	public void addChildNode(Character child , TrieNode node) {
		this.childNodes.put(child, node);
	}

	public eTypeOfNode getNodeType() {
		return nodeType;
	}

	public void setNodeType(eTypeOfNode nodeType) {
		this.nodeType = nodeType;
	}	
	
	public int getNumberOfChildNodes(){
		return this.childNodes.size();
		
	}
}
