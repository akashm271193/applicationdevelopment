import java.io.FileInputStream;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.OpenOption;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.util.Iterator;
import java.util.Scanner;
import java.util.TreeSet;

/**
 * Class: Main
 * @author AK
 * @description: Handles functionalities of file reading and storing data 
 */
public class Main {
	TreeSet<String> wordSet = new TreeSet<>();
	
	
	/**
	 * Function: main
	 * @description Opens a FileInputStream to a given file and parses it line by line showing the mem usage simultaneously
	 * @param String[] args
	 */
	public static void main(String[] args){
		
		/**
		 * Code for Method 1 - Using A TreeSet Data Structure
		 */
		/*try  {
			System.out.println("Running Tree Set Solution");
			Main mClass = new Main();
			Memory memClass = new Memory();
			memClass.getMemoryStats();
			long startTime = System.currentTimeMillis();
			Scanner sc = new Scanner(new FileInputStream("TestData.txt"),"UTF-8");
			memClass.getMemoryStats();
	        while (sc.hasNextLine()) {
	        	String stringToAdd = sc.nextLine();
	            mClass.addWordsToSet(stringToAdd);
	            //memClass.getMemoryStats();
	        }
	        memClass.getMemoryStats();
	        sc.close();
			long endTime = System.currentTimeMillis();
			System.out.println("Writing took " + (endTime - startTime)/1000 + " seconds");
			startTime = System.currentTimeMillis();
			mClass.getAllUniqueWordsFromSet();
			endTime = System.currentTimeMillis();
			System.out.println("Reading and dumping took " + (endTime - startTime)/1000 + " seconds");
			//System.out.println(mClass.wordSet.toString());
	        
	    } catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}*/
		
		
		/**
		 * Code for Method 2 - Using A Trie Data Structure
		 */
		try  {
			System.out.println("Running Trie Structure Solution");
			Memory memClass = new Memory();
			Trie wordsListTire = new Trie(true);
			memClass.getMemoryStats();
			long startTime = System.currentTimeMillis();
			Scanner sc = new Scanner(new FileInputStream("TestData.txt"),"UTF-8");
			memClass.getMemoryStats();
	        while (sc.hasNextLine()) {
	        	String stringToAdd = sc.nextLine();
	        	wordsListTire.addSentenceToTrie(stringToAdd);
	        }
	        memClass.getMemoryStats();
	        sc.close();
			long endTime = System.currentTimeMillis();
			System.out.println("Writing in the Trie took " + (endTime - startTime)/1000 + " seconds");
			startTime = System.currentTimeMillis();
			wordsListTire.getAllUniqueWordsFromTrie();
			endTime = System.currentTimeMillis();
			System.out.println("Reading and dumping into file took " + (endTime - startTime)/1000 + " seconds");
	    } catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	/**
	 * Function: addWordsToSet
	 * @description Adds given String data as words in a TreeSet
	 * @param inputStr
	 */
	public void addWordsToSet(String inputStr){
		String[] wordArr = inputStr.split(" ");
		for(String wordStr : wordArr){
			wordStr = wordStr.toLowerCase();
			wordStr = wordStr.replaceAll("[^a-zA-Z0-9$']","");
			wordSet.add(wordStr);
		}
	}
	
	
	/**
	 * Function: getAllUniqueWordsFromSet
	 * @throws IOException 
	 * @description Parses the set and writes the output in the file
	 */
	public void getAllUniqueWordsFromSet() throws IOException{
		Iterator<String> it = wordSet.iterator();
		Path path = Paths.get("OutputTestData.txt");
		OpenOption[] options = new OpenOption[] { StandardOpenOption.CREATE,StandardOpenOption.APPEND};
		Files.deleteIfExists(path);
		while (it.hasNext()){
			String word = it.next() + System.lineSeparator();
			try {
				Files.write(path, word.getBytes(), options);
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}
}