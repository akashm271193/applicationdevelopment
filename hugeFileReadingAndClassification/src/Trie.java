import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.OpenOption;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.Set;

/**
 * class: Trie
 * @author AK
 * @description: Base class for Trie Data Structure; Consists of rootNode & and an arraylist of words as members;
 */

public class Trie {
	private TrieNode rootTrieNode;
	ArrayList<String> wordList;
	private boolean cleanData;
	
	/*public static void main(String[] args){
		String s[] = new String[]{"(\"a\",\"aa\",\"aaaa\").\"","(suPPerman's","(Ozymandias),","�[It's]","researcher.[34][35]"};
		for(String str:s){
			str = str.replaceAll("[^a-zA-Z0-9$']","");
			System.out.println(str);
		}
	}*/
	
	public Trie(boolean toCleanData){
		this.rootTrieNode = new TrieNode(eTypeOfNode.ROOT);
		this.cleanData = toCleanData;
	}
	
	/**
	 * @author AK
	 * @param strWord
	 * @description: Adds a given word to the trie Data structure by iterating each character and checking if it is present at that 
	 * level of the Trie; else will add a new node to the Trie.
	 */
	public void addWordToTrie(String strWord){
		if(cleanData){
			strWord = cleanData(strWord);
		}
		TrieNode current = this.rootTrieNode;
		for(int counter = 0; counter < strWord.length(); counter++){ 
		  current = current.getChildNodes().computeIfAbsent(strWord.charAt(counter), c -> new TrieNode()); 
		}
		
		current.setNodeType(eTypeOfNode.WORDEND);
	}
	
	
	/**
	 * @author AK
	 * @param strWord
	 * @description: Explodes a given string into String[] based on spaces(" ");
	 */
	public void addSentenceToTrie(String strWord){
		String[] wordArr = strWord.split(" ");
		for(String wordStr : wordArr){
			addWordToTrie(wordStr);
		}
	}
	
	
	/**
	 * @author AK
	 * @throws IOException 
	 * @description: Parses the trie and writes the output in the file
	 */
	public void getAllUniqueWordsFromTrie() throws IOException{
		parseTrie();
		Path path = Paths.get("OutputTestData.txt");
		OpenOption[] options = new OpenOption[] { StandardOpenOption.CREATE,StandardOpenOption.APPEND};
		Files.deleteIfExists(path);
		
		for(String word:wordList){
			try {
				word += System.lineSeparator();
				Files.write(path, word.getBytes(), options);
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}
	
	/**
	 * @author AK
	 * @description: Parses the trie and looping over the nodes at the first level of the Trie and call getAllNodes for each of them 
	 */
	private void parseTrie(){
		TrieNode current = this.rootTrieNode;
		wordList = new ArrayList<>();
		Set<Character> keys = current.getChildNodes().keySet();
		Iterator<Character> it = keys.iterator();
		while(it.hasNext()){
			Character tmpChar = it.next();
			getAllNodes(tmpChar,"", current.getChildNodes().get(tmpChar));
		}
	}
	
	/**
	 * @author AK
	 * @param inChar
	 * @param wordStr
	 * @param curNode
	 * @description: Get nodes recursively at every level for a given node and return if wordEnd is found and then selects
	 * the next branch
	 */
	private void getAllNodes(Character inChar, String wordStr, TrieNode curNode){
		wordStr += inChar;
		if(curNode.getNodeType() == eTypeOfNode.WORDEND){
			wordList.add(wordStr);
			
			if(curNode.getNumberOfChildNodes() == 0)
				return;
		}
		
		Set<Character> keys = curNode.getChildNodes().keySet();
		Iterator<Character> it = keys.iterator();
		while(it.hasNext()){
			Character tmpChar = it.next();
			getAllNodes(tmpChar,wordStr,curNode.getChildNodes().get(tmpChar));
		}
	}
	
	/**
	 * @author AK
	 * @param inputData
	 * @description: Clean every word coming in as the input;
	 */
	private String cleanData(String inputData){
		String filteredData = inputData;
		filteredData = filteredData.toLowerCase();
		filteredData = filteredData.replaceAll("[^a-zA-Z0-9$']","");
		return filteredData;
	}
}